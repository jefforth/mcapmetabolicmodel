# generated with VANTED V2.2.1 at Wed Oct 04 15:51:33 PDT 2017
graph [
  directed 1
  node [
    id 1
    zlevel -1

    graphics [
      x 1110.0
      y 100.0
      w 40.0
      h 25.0
      fill "#FFFF00"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>methane [e]<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    graphics [
      x 1110.0
      y 222.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    graphics [
      x 1110.0
      y 492.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    graphics [
      x 1110.5
      y 342.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>methane [p]<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    graphics [
      x 900.0
      y 890.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    graphics [
      x 900.0
      y 740.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>O<sub>2</sub> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    graphics [
      x 780.0
      y 890.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    graphics [
      x 780.0
      y 740.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    graphics [
      x 840.0
      y 822.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "pMMO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    graphics [
      x 1052.0
      y 894.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    graphics [
      x 1170.0
      y 730.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    graphics [
      x 1171.0
      y 894.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    graphics [
      x 1050.0
      y 740.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>O<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    graphics [
      x 1110.0
      y 822.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "sMMO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    graphics [
      x 1170.0
      y 770.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    graphics [
      x 1110.3333333333333
      y 648.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>methane<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    graphics [
      x 1110.0
      y 1010.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>methanol<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    graphics [
      x 901.0
      y 1934.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    graphics [
      x 900.0
      y 1790.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "tetrahydromethanopterin (HMPT)"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    graphics [
      x 840.0
      y 1862.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA2866 or<br><center> MCA2778"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    graphics [
      x 840.25
      y 1981.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "5,10-methylene-HMPT"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    graphics [
      x 901.0
      y 2454.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    graphics [
      x 900.0
      y 2310.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    graphics [
      x 840.0
      y 2382.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "mch"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    graphics [
      x 840.0
      y 2260.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "5,10-methenyl-HMPT"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    graphics [
      x 901.0
      y 2704.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "tetrahydromethanopterin (HMPT)"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    graphics [
      x 900.0
      y 2560.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "methanofuran"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    graphics [
      x 782.0
      y 2704.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    graphics [
      x 840.0
      y 2632.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ffsA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    graphics [
      x 840.5
      y 2507.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "5-formyl-HMPT"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    graphics [
      x 781.0
      y 2954.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "methanofuran"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    graphics [
      x 780.0
      y 2810.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    graphics [
      x 840.0
      y 2882.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA2857 + MCA2860 +<br><center> (MCA2859 or MCA2319)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    graphics [
      x 840.5
      y 2757.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "formylmethanofuran"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    graphics [
      x 1566.9423076923078
      y 2781.903846153846
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    graphics [
      x 1421.9423076923078
      y 2781.903846153846
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    graphics [
      x 1493.442
      y 2839.904
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA3018-9 or<br><center>MCA0508"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    graphics [
      x 1316.9423076923078
      y 2781.9038461538466
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    graphics [
      x 1171.9423076923078
      y 2781.9038461538466
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    graphics [
      x 1243.4423076923078
      y 2839.9038461538466
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "fchA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    graphics [
      x 1368.942
      y 2840.404
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>5,10-methenyl-THF<br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    graphics [
      x 1118.9423076923078
      y 2840.4038461538466
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "10-formyl-THF"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    graphics [
      x 510.0
      y 2260.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    graphics [
      x 392.0
      y 2404.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    graphics [
      x 392.0
      y 2259.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    graphics [
      x 511.0
      y 2404.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    graphics [
      x 450.0
      y 2332.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "sqr"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    graphics [
      x 1030.0
      y 3180.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    graphics [
      x 970.0
      y 3252.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA1137-42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    graphics [
      x 840.125
      y 3110.5
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "formate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    graphics [
      x 1830.0
      y 1590.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA2739 or <br><center> MCA3043 or<br><center> MCA3049"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    graphics [
      x 840.0
      y 1590.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>formaldehyde<br><br>"
    labelgraphics [
      alignment "center"
      anchor "sw"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    graphics [
      x 2590.0
      y 1590.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA3044 or<br><center>MCA3050"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    graphics [
      x 2460.0
      y 1590.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>D-arabino-3-hexulose-6-phosphate<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    graphics [
      x 2770.0
      y 1640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>PP<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    graphics [
      x 2771.0
      y 1784.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    graphics [
      x 2652.0
      y 1784.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    graphics [
      x 2710.0
      y 1712.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "pfp"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    graphics [
      x 2710.0
      y 1962.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "fbaA-1 or fbaA-2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    graphics [
      x 2710.0
      y 1830.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>D-fructose-1,6-bisphosphate<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    graphics [
      x 2810.0
      y 2090.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp dihydroxyacetone<br><center>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp -phosphate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    graphics [
      x 2430.0
      y 2000.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "tkt-1 or tkt-2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    graphics [
      x 1991.429
      y 1842.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "tkt-1 or tkt-2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    graphics [
      x 2211.429
      y 1842.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "tal"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    graphics [
      x 1760.0
      y 1760.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "rpiA or rpiB"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    graphics [
      x 1760.0
      y 1920.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "rpe"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    graphics [
      x 2100.0
      y 1760.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>sedoheptulose-7-phosphate<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    graphics [
      x 2710.0
      y 1462.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "pgi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    graphics [
      x 2320.0
      y 1920.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "D-erythrose-4-phosphate"
    labelgraphics [
      alignment "center"
      anchor "ne"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    graphics [
      x 2710.411
      y 1589.149
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>D-fructose-6-phosphate<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    graphics [
      x 1880.0
      y 1760.0
      w 40.0
      h 40.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>D-ribose-5-phosphate<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    graphics [
      x 2710.0
      y 2090.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "tpiA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    graphics [
      x 2550.0
      y 2270.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    graphics [
      x 2670.0
      y 2270.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    graphics [
      x 2670.0
      y 2140.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    graphics [
      x 2552.0
      y 2139.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    graphics [
      x 2610.0
      y 2210.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "gap"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    graphics [
      x 2671.0
      y 2534.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    graphics [
      x 2670.0
      y 2390.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    graphics [
      x 2610.0
      y 2462.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "pgk"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    graphics [
      x 2610.5
      y 2337.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>1,3-Bisphospho-D-glycerate<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    graphics [
      x 2610.0
      y 2712.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "gpmA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    graphics [
      x 2671.0
      y 3034.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    graphics [
      x 2610.0
      y 2962.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "eno1 or eno2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    graphics [
      x 2550.0
      y 3220.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    graphics [
      x 2800.0
      y 3200.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    graphics [
      x 2682.0
      y 3199.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    graphics [
      x 2801.0
      y 3344.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    graphics [
      x 2551.0
      y 3344.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    graphics [
      x 2490.0
      y 3272.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ppdK"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    graphics [
      x 2740.0
      y 3272.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "pyk"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    graphics [
      x 2550.0
      y 3180.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>PP<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    graphics [
      x 2610.6665000000003
      y 3086.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "phosphoenolpyruvate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    graphics [
      x 3381.0
      y 1554.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    graphics [
      x 3320.0
      y 1482.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "edd"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    graphics [
      x 2542.0
      y 3499.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    graphics [
      x 2670.0
      y 3640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    graphics [
      x 2670.0
      y 3500.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    graphics [
      x 2540.0
      y 3640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    graphics [
      x 2610.0
      y 3570.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "aceEF + lpdA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    graphics [
      x 1691.0
      y 2034.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    graphics [
      x 1572.0
      y 2034.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>ADP<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    graphics [
      x 1572.0
      y 1889.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>ATP<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    graphics [
      x 1630.0
      y 1962.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "cbbP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    graphics [
      x 1630.0
      y 1840.0
      w 40.0
      h 40.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>D-ribulose-5-phosphate<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    graphics [
      x 1765.7588473701594
      y 2293.902942999379
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    graphics [
      x 1663.22836409811
      y 2191.3724597273294
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    graphics [
      x 1747.3740710593092
      y 2108.640966328503
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    graphics [
      x 1756.2129058241408
      y 2202.332614835721
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "cbbLS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    graphics [
      x 1630.0
      y 2080.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>D-ribulose-1,5-bisphosphate<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    graphics [
      x 1880.0
      y 1920.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>D-xylulose-5-phosphate &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    graphics [
      x 2610.0
      y 2090.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>glyceraldehyde &nbsp&nbsp&nbsp&nbsp&nbsp<br><center>-3-phosphate &nbsp&nbsp&nbsp&nbsp&nbsp <br><br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    graphics [
      x 2962.929
      y 1400.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    graphics [
      x 2962.929
      y 1281.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADPH<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    graphics [
      x 2810.0
      y 1280.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    graphics [
      x 2890.0
      y 1340.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>zwf-1 or<br><center> zwf-2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    graphics [
      x 3271.0
      y 1282.75
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    graphics [
      x 3127.0
      y 1283.75
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    graphics [
      x 3200.0
      y 1340.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>pgl-1 or<br><center>pgl-2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    graphics [
      x 3050.0
      y 1340.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>6-phospho-D-glucono-1,5-lactone<br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    graphics [
      x 3320.0
      y 1732.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "eda"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    graphics [
      x 3320.5
      y 1607.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "2-dehydro-3-deoxy-D-gluconate-6-phosphate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    graphics [
      x 3522.929
      y 1400.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    graphics [
      x 3522.929
      y 1281.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    graphics [
      x 3377.929
      y 1400.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    graphics [
      x 3451.429
      y 1342.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "gnd"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    graphics [
      x 3320.0
      y 1340.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "     6-phospho-D-gluconate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    graphics [
      x 840.0
      y 3512.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    graphics [
      x 840.0
      y 3390.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    graphics [
      x 840.0
      y 3880.0
      w 40.0
      h 25.0
      fill "#FFFF00"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub> [e]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    graphics [
      x 840.0
      y 3760.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    graphics [
      x 840.5
      y 3637.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    graphics [
      x 2540.0
      y 3960.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    graphics [
      x 2690.0
      y 3960.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    graphics [
      x 2700.0
      y 3820.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    graphics [
      x 3000.0
      y 3990.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    graphics [
      x 3060.0
      y 4110.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    graphics [
      x 2956.869
      y 4479.505999999999
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA3071"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    graphics [
      x 2610.697
      y 4679.916
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>succinyl-CoA<br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    graphics [
      x 3004.063
      y 4209.99
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA2485"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    graphics [
      x 2747.476
      y 4655.631
      w 60.0
      h 40.0
      fill "#F2F2FF"
      outline "#999999"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "sucAB + lpdA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#999999"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    graphics [
      x 2353.471
      y 4586.639
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "succinate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    graphics [
      x 3004.227
      y 4348.909
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "isocitrate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    graphics [
      x 2473.86
      y 4655.956
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>sucCD-1 or<br><center>sucCD-2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    graphics [
      x 2264.049
      y 4480.327
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "sdhAB + MCA1548-9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    graphics [
      x 2867.7
      y 4586.029
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "α-ketoglutarate"
    labelgraphics [
      alignment "center"
      anchor "se"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    graphics [
      x 2216.217039282904
      y 4210.924523512866
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "fumC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    graphics [
      x 2352.744
      y 3973.804
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "mdh"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    graphics [
      x 2866.973762324605
      y 3973.1942405953823
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA2485"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    graphics [
      x 2956.395
      y 4079.506
      w 40.0
      h 20.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>cis-aconitate<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    graphics [
      x 2216.382
      y 4349.843
      w 40.0
      h 20.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "fumarate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    graphics [
      x 2609.748
      y 3879.917
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "gltA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    graphics [
      x 2746.585
      y 3903.878
      w 40.0
      h 20.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>citrate<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    graphics [
      x 2970.0
      y 4560.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    graphics [
      x 2920.0
      y 4380.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    graphics [
      x 2850.0
      y 4500.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    graphics [
      x 2870.0
      y 4690.0
      w 25.0
      h 40.0
      fill "#FFF2E5"
      outline "#999999"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#999999"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    graphics [
      x 2680.0
      y 4750.0
      w 25.0
      h 25.0
      fill "#FFF2E5"
      outline "#999999"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#999999"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    graphics [
      x 2780.0
      y 4540.0
      w 25.0
      h 25.0
      fill "#FFF2E5"
      outline "#999999"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#999999"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    graphics [
      x 2650.0
      y 4580.0
      w 25.0
      h 25.0
      fill "#FFF2E5"
      outline "#999999"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#999999"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    graphics [
      x 2570.0
      y 4580.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    graphics [
      x 2440.0
      y 4540.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    graphics [
      x 2550.0
      y 4750.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    graphics [
      x 2340.0
      y 4690.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CoA<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    graphics [
      x 2240.0
      y 4600.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    graphics [
      x 2150.0
      y 4430.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 2130.0
      y 4300.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    graphics [
      x 2280.0
      y 3990.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    graphics [
      x 2380.0
      y 3850.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    graphics [
      x 2340.0
      y 3850.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    graphics [
      x 2431.0
      y 3344.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    graphics [
      x 2430.0
      y 3200.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "AMP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    graphics [
      x 1989.25
      y 2841.15
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "sgaA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    graphics [
      x 2310.75
      y 2901.1499999999996
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    graphics [
      x 2165.75
      y 2901.1499999999996
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    graphics [
      x 2166.75
      y 2783.1499999999996
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    graphics [
      x 2239.25
      y 2843.1499999999996
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA1407"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    graphics [
      x 2106.75
      y 2842.1499999999996
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "hydroxypyruvate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    graphics [
      x 2610.0
      y 2842.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "2-phospho-D-glycerate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    graphics [
      x 1617.442
      y 2840.154
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "5,10-methylene-THF"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    graphics [
      x 2471.484
      y 3902.101
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "oxaloacetate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    graphics [
      x 1860.0
      y 3820.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA1739"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    graphics [
      x 1988.786
      y 4023.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    graphics [
      x 2133.786
      y 4023.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    graphics [
      x 1988.786
      y 4142.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    graphics [
      x 2132.786
      y 4141.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    graphics [
      x 2060.0
      y 4080.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "sucCD-2"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    graphics [
      x 2260.0
      y 4080.0
      w 40.0
      h 40.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-malate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    graphics [
      x 1860.0
      y 4080.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>2S-4-malyl-CoA<br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    graphics [
      x 1860.0
      y 2950.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "glyoxylate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    graphics [
      x 2500.0
      y 2640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>ADP<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    graphics [
      x 2388.176
      y 2720.969
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>ATP<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 2481.867
      y 2712.131
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA1408"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    graphics [
      x 2361.875
      y 2841.325
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>R-glycerate<br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    graphics [
      x 2550.0
      y 2760.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    graphics [
      x 2410.0
      y 2660.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    graphics [
      x 2398.786
      y 2533.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    graphics [
      x 2543.786
      y 2533.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    graphics [
      x 2470.286
      y 2591.071
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA1407 or<br><center>MCA2968"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    graphics [
      x 2148.786
      y 2533.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>α-ketoglutarate<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    graphics [
      x 2293.786
      y 2533.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>L-glutamate<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    graphics [
      x 2220.0
      y 2590.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "serC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    graphics [
      x 2345.786
      y 2591.571
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "3-phosphohydroxypyruvate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    graphics [
      x 1918.352
      y 2691.596
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    graphics [
      x 2020.882
      y 2589.066
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    graphics [
      x 2009.922
      y 2682.051
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "serB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    graphics [
      x 2100.0
      y 2590.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "O-phospho-L-serine"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    graphics [
      x 2610.062
      y 2589.625
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "3-phospho-D-glycerate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    graphics [
      x 1860.0
      y 2840.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-serine"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    graphics [
      x 3002.929
      y 3510.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    graphics [
      x 2857.929
      y 3510.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    graphics [
      x 2931.429
      y 3452.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvBN or ilvK"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    graphics [
      x 3252.929
      y 3600.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    graphics [
      x 3107.929
      y 3600.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    graphics [
      x 3181.429
      y 3542.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "budA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    graphics [
      x 2610.021
      y 3449.875
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "pyruvate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    graphics [
      x 3002.929
      y 3810.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    graphics [
      x 3002.929
      y 3691.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    graphics [
      x 2857.929
      y 3810.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    graphics [
      x 2858.929
      y 3692.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    graphics [
      x 2931.429
      y 3752.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA0775 or <br><center>MCA1575"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    graphics [
      x 3252.929
      y 3810.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    graphics [
      x 3108.929
      y 3692.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    graphics [
      x 3107.929
      y 3810.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    graphics [
      x 3181.429
      y 3752.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA0775 or <br><center>MCA1575"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    graphics [
      x 3055.929
      y 3752.286
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "acetaldehyde"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    graphics [
      x 2610.0
      y 3690.0
      w 40.0
      h 40.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "acetyl-CoA"
    labelgraphics [
      alignment "center"
      anchor "se"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    graphics [
      x 2801.0
      y 3644.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>formate<br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    graphics [
      x 2800.0
      y 3500.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CoA<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    graphics [
      x 2740.0
      y 3570.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "? + MCA0073"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    graphics [
      x 982.0
      y 3584.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    graphics [
      x 982.0
      y 3439.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    graphics [
      x 1101.0
      y 3584.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    graphics [
      x 1100.0
      y 3440.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    graphics [
      x 1040.0
      y 3512.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA0163-6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    graphics [
      x 1039.464
      y 3391.393
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    graphics [
      x 1480.0
      y 100.0
      w 40.0
      h 25.0
      fill "#FFFF00"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>N<sub>2</sub> [e]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    graphics [
      x 1480.5
      y 342.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>N<sub>2</sub> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    graphics [
      x 1480.0
      y 222.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    graphics [
      x 1480.0
      y 462.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    graphics [
      x 1541.0
      y 874.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(16) P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    graphics [
      x 1540.0
      y 690.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "(16) ATP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    graphics [
      x 1422.0
      y 709.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "(8) ferredoxin - red"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    graphics [
      x 1420.0
      y 830.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "(8) ferredoxin - ox"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    graphics [
      x 1450.0
      y 940.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) NH<sub>4</sub><sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    graphics [
      x 1480.0
      y 782.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "nifHDKENXQ + MCA0232 + MCA0236-7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    graphics [
      x 1540.0
      y 730.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(16) H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    graphics [
      x 1540.0
      y 830.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "(16) ADP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    graphics [
      x 1520.0
      y 940.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    graphics [
      x 1420.0
      y 870.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(6) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    graphics [
      x 1480.0
      y 590.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>N<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    graphics [
      x 2652.0
      y 1294.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    graphics [
      x 2652.0
      y 1149.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    graphics [
      x 2771.0
      y 1294.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    graphics [
      x 2710.0
      y 1100.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "D-glucose"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    graphics [
      x 2710.0
      y 1222.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "glk"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    graphics [
      x 2710.0
      y 1340.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>D-glucose-6-phosphate<br>"
    labelgraphics [
      alignment "center"
      anchor "se"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    graphics [
      x 3301.464
      y 3540.893
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>R-acetoin<br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    graphics [
      x 3301.464
      y 3750.893
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ethanol"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    graphics [
      x 3120.0
      y 3300.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    graphics [
      x 3140.0
      y 3430.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    graphics [
      x 3250.0
      y 3300.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    graphics [
      x 3180.0
      y 3360.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    graphics [
      x 3480.0
      y 3440.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    graphics [
      x 3430.0
      y 3360.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    graphics [
      x 3300.0
      y 3360.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "R-2,3-dihydroxy-3-methylbutanoate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    graphics [
      x 3050.0
      y 3450.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "S-2-acetolactate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    graphics [
      x 3610.0
      y 3300.0
      w 25.0
      h 80.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    graphics [
      x 3490.714
      y 3169.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    graphics [
      x 3491.714
      y 3313.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "acetyl-CoA"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    graphics [
      x 3609.714
      y 3169.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    graphics [
      x 3550.7142857142853
      y 3119.857142857143
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "3-carboxy-3-hydroxy-4-methylpentanoate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    graphics [
      x 3551.714
      y 3241.357
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "leuA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    graphics [
      x 3611.0
      y 2824.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    graphics [
      x 3550.0
      y 2630.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "3-carboxy-2-hydroxy-4-methylpentanoate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    graphics [
      x 3550.0
      y 2752.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "leuCD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    graphics [
      x 3550.0
      y 2870.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "2-isopropylmaleate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    graphics [
      x 3550.0
      y 2992.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "leuCD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    graphics [
      x 3490.714
      y 2439.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    graphics [
      x 3491.714
      y 2583.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    graphics [
      x 3551.7142857142853
      y 2511.357142857143
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "leuB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    graphics [
      x 3742.929
      y 2331.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    graphics [
      x 3548.929
      y 2392.786
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>3-carboxy-4-methyl-2-<br><center>oxopentanoate"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    graphics [
      x 3671.429
      y 2392.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "spontaneous"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    graphics [
      x 3848.9285714285716
      y 2332.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    graphics [
      x 3992.9285714285716
      y 2331.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "α-ketoglutarate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 287
    zlevel -1

    graphics [
      x 4042.9285714285716
      y 2391.7857142857138
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-leucine"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 288
    zlevel -1

    graphics [
      x 3798.9285714285716
      y 2392.7857142857138
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "4-methyl-2-oxopentanoate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 289
    zlevel -1

    graphics [
      x 3921.4285714285716
      y 2392.7857142857138
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 290
    zlevel -1

    graphics [
      x 3610.0
      y 2920.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 291
    zlevel -1

    graphics [
      x 3598.9285714285716
      y 2332.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 292
    zlevel -1

    graphics [
      x 3610.0
      y 2440.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 293
    zlevel -1

    graphics [
      x 3021.0
      y 2314.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 294
    zlevel -1

    graphics [
      x 2961.0
      y 2364.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "methylglyoxal"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 295
    zlevel -1

    graphics [
      x 2960.0
      y 2242.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "spontaneous"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 296
    zlevel -1

    graphics [
      x 3020.0
      y 2410.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "glutathione-red"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 297
    zlevel -1

    graphics [
      x 2960.0
      y 2482.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "gloA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 298
    zlevel -1

    graphics [
      x 3021.0
      y 2804.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "glutathione-red"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 299
    zlevel -1

    graphics [
      x 3020.0
      y 2660.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 300
    zlevel -1

    graphics [
      x 2902.0
      y 2804.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 301
    zlevel -1

    graphics [
      x 2961.0
      y 2854.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "D-lactate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 302
    zlevel -1

    graphics [
      x 2960.0
      y 2610.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "R-S-lactoylglutathione"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 303
    zlevel -1

    graphics [
      x 2960.0
      y 2732.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "gloB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 304
    zlevel -1

    graphics [
      x 1572.9285714285716
      y 4061.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 305
    zlevel -1

    graphics [
      x 1428.9285714285716
      y 4062.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "α-ketoglutarate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 306
    zlevel -1

    graphics [
      x 1378.9285714285716
      y 4122.785714285714
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-aspartate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 307
    zlevel -1

    graphics [
      x 1501.4285714285716
      y 4122.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA2202"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 308
    zlevel -1

    graphics [
      x 1322.9285714285716
      y 4061.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 309
    zlevel -1

    graphics [
      x 1178.9285714285716
      y 4062.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 310
    zlevel -1

    graphics [
      x 1128.929
      y 4122.786
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>4-phospho<br>-L-aspartate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 311
    zlevel -1

    graphics [
      x 1251.4285714285716
      y 4122.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA0390"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 312
    zlevel -1

    graphics [
      x 1082.9285714285716
      y 4061.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 313
    zlevel -1

    graphics [
      x 937.9285714285716
      y 4190.785714285715
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 314
    zlevel -1

    graphics [
      x 1082.9285714285716
      y 4190.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 315
    zlevel -1

    graphics [
      x 938.9285714285716
      y 4062.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 316
    zlevel -1

    graphics [
      x 888.9285714285716
      y 4122.785714285714
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html><center>L-aspartate<br>-4-semialdehyde"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 317
    zlevel -1

    graphics [
      x 1011.4285714285716
      y 4122.785714285714
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "asd"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 318
    zlevel -1

    graphics [
      x 832.9285714285716
      y 4061.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 319
    zlevel -1

    graphics [
      x 832.9285714285716
      y 4190.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 320
    zlevel -1

    graphics [
      x 688.9285714285716
      y 4062.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 321
    zlevel -1

    graphics [
      x 638.9285714285716
      y 4122.785714285714
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-homoserine"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 322
    zlevel -1

    graphics [
      x 761.4285714285716
      y 4122.785714285714
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "hom"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 323
    zlevel -1

    graphics [
      x 438.786
      y 4182.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 324
    zlevel -1

    graphics [
      x 438.786
      y 4063.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 325
    zlevel -1

    graphics [
      x 583.786
      y 4063.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 326
    zlevel -1

    graphics [
      x 510.28571428571445
      y 4121.0714285714275
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA0600 or<br><center>MCA2303"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 327
    zlevel -1

    graphics [
      x 332.0
      y 4324.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 328
    zlevel -1

    graphics [
      x 332.0
      y 4179.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "H2O"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 329
    zlevel -1

    graphics [
      x 390.0
      y 4120.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "O-phospho-L-homoserine"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 330
    zlevel -1

    graphics [
      x 390.0
      y 4252.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "thrC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 331
    zlevel -1

    graphics [
      x 1322.9285714285716
      y 4430.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 332
    zlevel -1

    graphics [
      x 1372.929
      y 4371.786
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "S-3-methyl-2-oxopentanoate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 333
    zlevel -1

    graphics [
      x 1250.0
      y 4370.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 334
    zlevel -1

    graphics [
      x 1427.9285714285716
      y 4430.785714285715
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 335
    zlevel -1

    graphics [
      x 1572.9285714285716
      y 4430.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "α-ketoglutarate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 336
    zlevel -1

    graphics [
      x 1622.9285714285716
      y 4371.785714285714
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-isoleucine"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 337
    zlevel -1

    graphics [
      x 1501.4285714285716
      y 4372.785714285714
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 338
    zlevel -1

    graphics [
      x 582.9285714285716
      y 4430.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NH<sub>4</sub><sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 339
    zlevel -1

    graphics [
      x 390.0
      y 4370.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-threonine"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 340
    zlevel -1

    graphics [
      x 510.0
      y 4370.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 341
    zlevel -1

    graphics [
      x 688.9285714285716
      y 4312.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 342
    zlevel -1

    graphics [
      x 832.9285714285716
      y 4430.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 343
    zlevel -1

    graphics [
      x 687.9285714285716
      y 4430.785714285715
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "pyruvate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 344
    zlevel -1

    graphics [
      x 760.0
      y 4370.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvBN or ilvK"
    labelgraphics [
      alignment "center"
      anchor "t"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 345
    zlevel -1

    graphics [
      x 1082.9285714285716
      y 4430.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 346
    zlevel -1

    graphics [
      x 937.9285714285716
      y 4430.785714285715
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 347
    zlevel -1

    graphics [
      x 938.9285714285716
      y 4312.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 348
    zlevel -1

    graphics [
      x 1130.0
      y 4370.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "R-2,3-dihydroxy-3-methylpentanoate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 349
    zlevel -1

    graphics [
      x 880.0
      y 4370.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html><center>S-2-aceto<br>-2-hydroxybutanoate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 350
    zlevel -1

    graphics [
      x 1010.0
      y 4370.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 351
    zlevel -1

    graphics [
      x 1440.0
      y 3800.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>PP<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 352
    zlevel -1

    graphics [
      x 1320.0
      y 3840.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamate"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 353
    zlevel -1

    graphics [
      x 1439.714
      y 3984.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 354
    zlevel -1

    graphics [
      x 1321.714
      y 3963.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamine"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 355
    zlevel -1

    graphics [
      x 1380.714
      y 3769.857
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-asparagine"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 356
    zlevel -1

    graphics [
      x 1381.7142857142858
      y 3891.357142857143
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA0472 or MCA0542 or MCA2127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 357
    zlevel -1

    graphics [
      x 1440.0
      y 3950.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 358
    zlevel -1

    graphics [
      x 1440.0
      y 3840.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "AMP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 359
    zlevel -1

    graphics [
      x 1320.0
      y 3800.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 360
    zlevel -1

    graphics [
      x 3310.714
      y 4709.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NH<sub>4</sub><sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 361
    zlevel -1

    graphics [
      x 3311.7142857142853
      y 4853.857142857143
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 362
    zlevel -1

    graphics [
      x 3370.0
      y 4580.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 363
    zlevel -1

    graphics [
      x 3370.0
      y 4990.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamine"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 364
    zlevel -1

    graphics [
      x 3371.7142857142853
      y 4781.357142857143
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "pabB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 365
    zlevel -1

    graphics [
      x 3482.0
      y 4854.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 366
    zlevel -1

    graphics [
      x 3601.0
      y 4844.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 367
    zlevel -1

    graphics [
      x 3482.0
      y 4709.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NH<sub>4</sub><sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 368
    zlevel -1

    graphics [
      x 3600.0
      y 4710.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 369
    zlevel -1

    graphics [
      x 3540.0
      y 4782.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "glnA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 370
    zlevel -1

    graphics [
      x 3141.7142857142853
      y 4853.857142857143
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 371
    zlevel -1

    graphics [
      x 3140.7142857142853
      y 4709.857142857143
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 372
    zlevel -1

    graphics [
      x 3259.714
      y 4854.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 373
    zlevel -1

    graphics [
      x 3201.7142857142853
      y 4781.357142857143
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "gltBD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 374
    zlevel -1

    graphics [
      x 3600.0
      y 4880.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 375
    zlevel -1

    graphics [
      x 3037.9285714285716
      y 4640.785714285715
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 376
    zlevel -1

    graphics [
      x 3182.9285714285716
      y 4640.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 377
    zlevel -1

    graphics [
      x 3182.9285714285716
      y 4521.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 378
    zlevel -1

    graphics [
      x 3030.0
      y 4520.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 379
    zlevel -1

    graphics [
      x 3070.0
      y 4520.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NH<sub>4</sub><sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 380
    zlevel -1

    graphics [
      x 3111.4285714285716
      y 4582.785714285714
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "gluD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 381
    zlevel -1

    graphics [
      x 840.0
      y 1010.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>methanol [p]<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 382
    zlevel -1

    graphics [
      x 980.0
      y 1010.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 383
    zlevel -1

    graphics [
      x 840.5
      y 1342.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "formaldehyde [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 384
    zlevel -1

    graphics [
      x 840.0
      y 1460.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 385
    zlevel -1

    graphics [
      x 762.0
      y 1254.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) cytochrome c<sub>L</sub><br> <center> - red [p]"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 386
    zlevel -1

    graphics [
      x 901.0
      y 1254.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 387
    zlevel -1

    graphics [
      x 762.0
      y 1109.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) cytochrome c<sub>L</sub> - ox[p]&nbsp&nbsp&nbsp&nbsp&nbsp"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 388
    zlevel -1

    graphics [
      x 840.0
      y 1182.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html> mxaFJGIRACKLD <br><center> or xoxFJ"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 389
    zlevel -1

    graphics [
      x 2878.111
      y 709.889
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) cytochrome c<sub>L</sub><br><center> - red [p]"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 390
    zlevel -1

    graphics [
      x 2879.1111111111113
      y 853.8888888888887
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) cytochrome c<sub>L</sub><br><center> - ox [p]"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 391
    zlevel -1

    graphics [
      x 2939.1111111111113
      y 781.3888888888887
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA0781 + ?"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 392
    zlevel -1

    graphics [
      x 3167.111
      y 709.889
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(0.5) O<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 393
    zlevel -1

    graphics [
      x 3018.111
      y 709.8890000000001
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) cytochrome c<sub>H</sub> - ox [p]"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 394
    zlevel -1

    graphics [
      x 3019.111
      y 853.8890000000001
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>2) cytochrome c<sub>H</sub> - red [p]&nbsp&nbsp&nbsp"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 395
    zlevel -1

    graphics [
      x 3167.111
      y 854.889
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 396
    zlevel -1

    graphics [
      x 3099.0
      y 924.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(4) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 397
    zlevel -1

    graphics [
      x 3099.1111111111113
      y 781.3888888888887
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>coxABC or<br><center>MCA2395-9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 398
    zlevel -1

    graphics [
      x 3480.0
      y 640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(4) H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 399
    zlevel -1

    graphics [
      x 3552.929
      y 721.7860000000001
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 400
    zlevel -1

    graphics [
      x 3480.0
      y 923.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(3) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 401
    zlevel -1

    graphics [
      x 3408.929
      y 722.7860000000001
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 402
    zlevel -1

    graphics [
      x 3350.0
      y 860.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 403
    zlevel -1

    graphics [
      x 3600.0
      y 870.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 404
    zlevel -1

    graphics [
      x 3481.429
      y 782.7860000000001
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "atpABCDEFGH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 405
    zlevel -1

    graphics [
      x 680.0
      y 1310.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 406
    zlevel -1

    graphics [
      x 680.0
      y 1180.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA0781 + ?"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 407
    zlevel -1

    graphics [
      x 2450.0
      y 1340.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "  D-glucose-1-phosphate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 408
    zlevel -1

    graphics [
      x 2580.0
      y 1340.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html><center>glmM or<br><center> pgm or<br><center>MCA2733 or<br><center>MCA2782"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 409
    zlevel -1

    graphics [
      x 2403.786
      y 1286.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 410
    zlevel -1

    graphics [
      x 2402.786
      y 1404.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 411
    zlevel -1

    graphics [
      x 2258.786
      y 1286.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>PP<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 412
    zlevel -1

    graphics [
      x 2200.0
      y 1340.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>ADP-glucose<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 413
    zlevel -1

    graphics [
      x 2330.0
      y 1340.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "glgC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 414
    zlevel -1

    graphics [
      x 2012.786
      y 1404.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 415
    zlevel -1

    graphics [
      x 2012.786
      y 1285.071
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 416
    zlevel -1

    graphics [
      x 1960.0
      y 1340.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "glycogen"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 417
    zlevel -1

    graphics [
      x 2080.0
      y 1340.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>glgA1 or<br><center>glgA2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 418
    zlevel -1

    graphics [
      x 2291.714
      y 923.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(4) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 419
    zlevel -1

    graphics [
      x 2290.714
      y 639.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(3) H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 420
    zlevel -1

    graphics [
      x 2451.0
      y 924.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 421
    zlevel -1

    graphics [
      x 2450.0
      y 640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(4) H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 422
    zlevel -1

    graphics [
      x 2230.0
      y 852.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 423
    zlevel -1

    graphics [
      x 2520.0
      y 710.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "cytochrome c - red"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 424
    zlevel -1

    graphics [
      x 2520.0
      y 853.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "cytochrome c - ox"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 425
    zlevel -1

    graphics [
      x 2370.857
      y 854.4290000000001
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 426
    zlevel -1

    graphics [
      x 2370.857
      y 709.4290000000001
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 427
    zlevel -1

    graphics [
      x 2230.714
      y 709.857
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 428
    zlevel -1

    graphics [
      x 2291.714
      y 781.357
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "nuoABCEFGHIJKLMN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 429
    zlevel -1

    graphics [
      x 2450.0
      y 782.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>cydAB or<br><center>petCBA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 430
    zlevel -1

    graphics [
      x 2590.0
      y 640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 431
    zlevel -1

    graphics [
      x 2591.0
      y 924.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(4) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 432
    zlevel -1

    graphics [
      x 2650.0
      y 850.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 433
    zlevel -1

    graphics [
      x 2650.0
      y 710.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(0.5) O<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 434
    zlevel -1

    graphics [
      x 2590.0
      y 782.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>coxABC or<br><center>MCA2395-9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 435
    zlevel -1

    graphics [
      x 1620.0
      y 2730.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "glycine"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 436
    zlevel -1

    graphics [
      x 1667.9285714285716
      y 2900.7857142857147
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 437
    zlevel -1

    graphics [
      x 1740.0
      y 2840.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 438
    zlevel -1

    graphics [
      x 1812.9285714285716
      y 2781.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "THF"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 439
    zlevel -1

    graphics [
      x 1188.139097744362
      y 4582.890977443605
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 440
    zlevel -1

    graphics [
      x 1380.0
      y 4680.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "pyruvate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 441
    zlevel -1

    graphics [
      x 833.1390977443621
      y 4582.890977443604
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 442
    zlevel -1

    graphics [
      x 1333.139097744362
      y 4582.890977443604
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 443
    zlevel -1

    graphics [
      x 688.1390977443621
      y 4701.890977443605
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 444
    zlevel -1

    graphics [
      x 1188.139097744362
      y 4701.890977443605
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 445
    zlevel -1

    graphics [
      x 688.1390977443621
      y 4582.890977443605
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 446
    zlevel -1

    graphics [
      x 1138.139097744362
      y 4641.890977443599
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "R-citramalate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 447
    zlevel -1

    graphics [
      x 888.1390977443621
      y 4641.890977443582
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "D-erythro-3-methylmalate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 448
    zlevel -1

    graphics [
      x 1380.0
      y 4600.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "acetyl-CoA"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 449
    zlevel -1

    graphics [
      x 1009.6390977443621
      y 4640.890977443605
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "leuCD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 450
    zlevel -1

    graphics [
      x 1259.639097744362
      y 4640.890977443602
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "leuA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 451
    zlevel -1

    graphics [
      x 332.0
      y 4564.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 452
    zlevel -1

    graphics [
      x 332.0
      y 4419.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 453
    zlevel -1

    graphics [
      x 350.0
      y 4850.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "acetyl-CoA"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 454
    zlevel -1

    graphics [
      x 451.0
      y 4564.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 455
    zlevel -1

    graphics [
      x 450.0
      y 4660.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 456
    zlevel -1

    graphics [
      x 391.0
      y 4614.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-2-amino-3-oxobutanoate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 457
    zlevel -1

    graphics [
      x 430.0
      y 4850.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "glycine"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 458
    zlevel -1

    graphics [
      x 390.0
      y 4732.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "bioF"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 459
    zlevel -1

    graphics [
      x 390.0
      y 4492.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA0775"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 460
    zlevel -1

    graphics [
      x 759.6390977443621
      y 4640.890977443604
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "leuB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 461
    zlevel -1

    graphics [
      x 630.0
      y 4370.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "2-oxobutanoate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 462
    zlevel -1

    graphics [
      x 902.0
      y 2194.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADPH<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 463
    zlevel -1

    graphics [
      x 902.0
      y 2049.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 464
    zlevel -1

    graphics [
      x 840.0
      y 2120.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA3018-9 or<br><center> MCA0508"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 465
    zlevel -1

    graphics [
      x 1180.0
      y 2970.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 466
    zlevel -1

    graphics [
      x 890.0
      y 2930.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "THF"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 467
    zlevel -1

    graphics [
      x 880.0
      y 2980.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 468
    zlevel -1

    graphics [
      x 1050.0
      y 3090.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "THF"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 469
    zlevel -1

    graphics [
      x 1150.0
      y 3010.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 470
    zlevel -1

    graphics [
      x 1010.0
      y 3120.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 471
    zlevel -1

    graphics [
      x 970.0
      y 2864.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 472
    zlevel -1

    graphics [
      x 970.0
      y 2944.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "fmt"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 473
    zlevel -1

    graphics [
      x 1050.0
      y 3010.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "fhs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 474
    zlevel -1

    graphics [
      x 2540.0
      y 3610.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 475
    zlevel -1

    graphics [
      x 2420.0
      y 3640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "       (2) flavodoxin - red"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 476
    zlevel -1

    graphics [
      x 2419.667
      y 3500.333
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "            (2) flavidoxin - ox"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 477
    zlevel -1

    graphics [
      x 2480.666666666667
      y 3571.833333333333
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA0769"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 478
    zlevel -1

    graphics [
      x 2270.0
      y 3640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 479
    zlevel -1

    graphics [
      x 2270.0
      y 3500.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>CO<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 480
    zlevel -1

    graphics [
      x 2140.0
      y 3640.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 481
    zlevel -1

    graphics [
      x 2140.0
      y 3490.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 482
    zlevel -1

    graphics [
      x 2200.0
      y 3570.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "sfcA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 483
    zlevel -1

    graphics [
      x 2340.0
      y 3570.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "oadABC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 484
    zlevel -1

    graphics [
      x 3530.0
      y 4080.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 485
    zlevel -1

    graphics [
      x 3412.0
      y 4224.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 486
    zlevel -1

    graphics [
      x 3232.0
      y 4079.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamate"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 487
    zlevel -1

    graphics [
      x 3410.0
      y 4100.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 488
    zlevel -1

    graphics [
      x 3410.0
      y 4060.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NH<sub>4</sub><sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 489
    zlevel -1

    graphics [
      x 3710.0
      y 4080.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-valine"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 490
    zlevel -1

    graphics [
      x 3531.0
      y 4224.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 491
    zlevel -1

    graphics [
      x 3232.0
      y 4224.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "α-ketoglutarate"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 492
    zlevel -1

    graphics [
      x 3711.0
      y 4224.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "2-ketoisovalerate"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 493
    zlevel -1

    graphics [
      x 3470.0
      y 3970.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "pyruvate"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 494
    zlevel -1

    graphics [
      x 3470.0
      y 4340.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-alanine"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 495
    zlevel -1

    graphics [
      x 3470.0
      y 4152.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ald"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 496
    zlevel -1

    graphics [
      x 3290.0
      y 4152.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA0598"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 497
    zlevel -1

    graphics [
      x 3650.0
      y 4152.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA2997"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 498
    zlevel -1

    graphics [
      x 2082.929
      y 2151.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 499
    zlevel -1

    graphics [
      x 2460.0
      y 2370.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "AMP"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 500
    zlevel -1

    graphics [
      x 2348.9285714285716
      y 2252.785714285714
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "CoA"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 501
    zlevel -1

    graphics [
      x 2347.9285714285716
      y 2370.7857142857147
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 502
    zlevel -1

    graphics [
      x 2460.0
      y 2250.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>PP<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 503
    zlevel -1

    graphics [
      x 2120.0
      y 2370.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 504
    zlevel -1

    graphics [
      x 1937.929
      y 2270.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>P<sub>i</sub>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 505
    zlevel -1

    graphics [
      x 2240.0
      y 2370.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 506
    zlevel -1

    graphics [
      x 2290.0
      y 2310.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "acetate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 507
    zlevel -1

    graphics [
      x 2071.464
      y 2310.393
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "acetyl-phosphate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 508
    zlevel -1

    graphics [
      x 2510.0
      y 2310.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "acetyl-CoA"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 509
    zlevel -1

    graphics [
      x 2011.429
      y 2212.786
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA1587"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 510
    zlevel -1

    graphics [
      x 2180.0
      y 2310.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html><center>ackA<br><center>or purT"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 511
    zlevel -1

    graphics [
      x 2400.0
      y 2310.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "acs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 512
    zlevel -1

    graphics [
      x 692.0
      y 3179.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 513
    zlevel -1

    graphics [
      x 692.0
      y 3324.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 514
    zlevel -1

    graphics [
      x 750.0
      y 3252.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "<html>MCA1389-93 or<br><center>MCA2576-7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 515
    zlevel -1

    graphics [
      x 510.0
      y 3550.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 516
    zlevel -1

    graphics [
      x 510.0
      y 3470.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 517
    zlevel -1

    graphics [
      x 510.0
      y 3430.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 518
    zlevel -1

    graphics [
      x 510.0
      y 3590.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 519
    zlevel -1

    graphics [
      x 570.5
      y 3387.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "formate [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 520
    zlevel -1

    graphics [
      x 570.0
      y 3512.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "MCA1208-10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 521
    zlevel -1

    graphics [
      x 570.0
      y 3262.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "diffusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 522
    zlevel -1

    graphics [
      x 1950.714
      y 709.857
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 523
    zlevel -1

    graphics [
      x 2070.857
      y 854.4290000000001
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 524
    zlevel -1

    graphics [
      x 2011.714
      y 781.357
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "pntAB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 525
    zlevel -1

    graphics [
      x 2070.857
      y 709.4290000000001
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 526
    zlevel -1

    graphics [
      x 2010.714
      y 639.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 527
    zlevel -1

    graphics [
      x 2011.714
      y 923.857
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>(2) H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 528
    zlevel -1

    graphics [
      x 1950.0
      y 852.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NAD<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 529
    zlevel -1

    graphics [
      x 1692.0
      y 709.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>H<sub>2</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 530
    zlevel -1

    graphics [
      x 1690.0
      y 850.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>Q<sub>8</sub>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 531
    zlevel -1

    graphics [
      x 1811.0
      y 854.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sub>2</sub>O [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 532
    zlevel -1

    graphics [
      x 1810.0
      y 710.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>O<sub>2</sub> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 533
    zlevel -1

    graphics [
      x 1751.0
      y 904.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "hydroxylamine [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 534
    zlevel -1

    graphics [
      x 1750.0
      y 660.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NH<sub>3</sub> [p]"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 535
    zlevel -1

    graphics [
      x 1750.0
      y 782.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "pMMO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 536
    zlevel -1

    graphics [
      x 3492.929
      y 3600.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>NADP<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 537
    zlevel -1

    graphics [
      x 3348.929
      y 3482.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 538
    zlevel -1

    graphics [
      x 3347.929
      y 3600.786
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 539
    zlevel -1

    graphics [
      x 3540.0
      y 3540.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "2,3-butanediol"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 540
    zlevel -1

    graphics [
      x 3420.0
      y 3540.0
      w 60.0
      h 40.0
      fill "#CCFF00"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "BDH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 541
    zlevel -1

    graphics [
      x 3752.9285714285716
      y 3441.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "α-ketoglutarate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 542
    zlevel -1

    graphics [
      x 3608.9285714285716
      y 3442.7857142857138
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-glutamate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 543
    zlevel -1

    graphics [
      x 3800.0
      y 3360.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "L-valine"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 544
    zlevel -1

    graphics [
      x 3550.0
      y 3360.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "2-ketoisovalerate"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 545
    zlevel -1

    graphics [
      x 3680.0
      y 3360.0
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "ilvE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 546
    zlevel -1

    graphics [
      x 1780.0
      y 3740.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>H<sup>+</sup>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  edge [
    id 1
    source 1
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 2
    source 4
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 3
    source 2
    target 4
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 4
    source 6
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 8
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 6
    source 9
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 9
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 8
    source 11
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 13
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 10
    source 14
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 11
    source 14
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 12
    source 15
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 3
    target 16
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 14
    source 19
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 20
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 20
    target 21
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 23
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 24
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 25
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 27
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 29
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 29
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 24
    target 30
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 30
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 32
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 33
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 29
    target 34
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 34
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 35
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 37
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 38
    target 40
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 40
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 37
    target 41
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 41
    target 40
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 40
    target 42
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 43
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 45
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 47
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 47
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 48
    target 49
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 33
    target 50
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 47
    target 50
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 450.0 y 2840.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 50
    target 49
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 950.0 y 3140.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 52
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 52
    target 20
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 52
    target 47
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 450.0 y 1590.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 51
    target 54
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 54
    target 53
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 55
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 58
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 58
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 58
    target 60
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 60
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 59
    target 61
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 63
    target 67
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 67
    target 64
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 69
    target 62
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 64
    target 69
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 62
    target 70
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2510.0 y 1980.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 68
    target 70
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 53
    target 70
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 70
    target 58
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 64
    target 70
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 65
    target 71
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 71
    target 63
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 72
    target 61
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 75
    target 77
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 76
    target 77
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 77
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 77
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 79
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 80
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 81
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 77
    target 81
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 84
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 85
    target 90
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "first"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 86
    target 91
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 87
    target 91
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 90
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "first"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 91
    target 88
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 90
    target 92
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 84
    target 93
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 93
    target 91
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2720.0 y 3140.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 93
    target 90
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2500.0 y 3140.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "first"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 95
    target 94
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 96
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 98
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 100
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 100
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 103
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 104
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 104
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 105
    target 51
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 105
    target 65
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 105
    target 66
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 105
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 107
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 108
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 109
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 104
    target 110
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 110
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 66
    target 111
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 62
    target 111
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2320.0 y 2070.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 111
    target 63
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 62
    target 112
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 59
    target 112
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 112
    target 77
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 112
    target 72
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 115
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 116
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 116
    target 114
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 118
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 119
    target 117
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 120
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 116
    target 120
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 122
    target 121
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 95
    target 122
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 125
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 126
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 126
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 119
    target 127
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 127
    target 95
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 127
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 126
    target 105
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3590.0 y 1340.0 ]
        point [ x 3600.0 y 1310.0 ]
        point [ x 3600.0 y 1040.0 ]
        point [ x 3540.0 y 1040.0 ]
        point [ x 1680.0 y 1040.0 ]
        point [ x 1630.0 y 1060.0 ]
        point [ x 1630.0 y 1390.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 121
    target 112
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2930.0 y 2100.0 ]
        point [ x 2800.0 y 2130.0 ]
        point [ x 2640.0 y 2120.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 49
    target 129
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 950.0 y 3360.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 64
    target 112
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2110.0 y 1910.0 ]
        point [ x 2110.0 y 1930.0 ]
        point [ x 2110.0 y 2080.0 ]
        point [ x 2110.0 y 2080.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 63
    target 112
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2090.0 y 1910.0 ]
        point [ x 2090.0 y 1930.0 ]
        point [ x 2090.0 y 2090.0 ]
        point [ x 2090.0 y 2100.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 152
    target 153
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 153
    target 149
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 149
    target 150
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 150
    target 140
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 140
    target 143
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 143
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 138
    target 146
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 146
    target 141
    graphics [
      fill "#000000"
      outline "#999999"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 141
    target 139
    graphics [
      fill "#000000"
      outline "#999999"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 139
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 144
    target 142
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 142
    target 145
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 145
    target 151
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 151
    target 147
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 133
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 152
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 152
    target 134
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 149
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 137
    target 140
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 155
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 138
    target 156
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 138
    target 154
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 159
    target 141
    graphics [
      fill "#000000"
      outline "#999999"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 157
    target 141
    graphics [
      fill "#000000"
      outline "#999999"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 141
    target 160
    graphics [
      fill "#000000"
      outline "#999999"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 141
    target 158
    graphics [
      fill "#000000"
      outline "#999999"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 161
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 163
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 144
    target 162
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 144
    target 164
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 165
    target 145
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 145
    target 166
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 167
    target 147
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 168
    target 148
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 148
    target 169
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 148
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 90
    target 172
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 171
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 175
    target 177
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 176
    target 177
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 177
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 178
    target 177
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 173
    target 178
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 82
    target 179
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 179
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 180
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 148
    target 181
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 181
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 184
    target 187
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 186
    target 187
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 187
    target 183
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 187
    target 185
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 147
    target 188
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 188
    target 148
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 188
    target 187
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 189
    target 182
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 187
    target 189
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 182
    target 190
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 190
    target 173
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 192
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 193
    target 191
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 194
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 177
    target 194
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 193
    target 195
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 198
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 199
    target 197
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 199
    target 196
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 201
    target 202
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 202
    target 200
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 203
    target 202
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 199
    target 203
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 205
    target 206
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 206
    target 204
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 207
    target 206
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 202
    target 207
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 208
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 193
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 80
    target 208
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 208
    target 82
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 206
    target 209
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 209
    target 173
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 109
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2100.0 y 2460.0 ]
        point [ x 2360.0 y 2480.0 ]
        point [ x 2540.0 y 2480.0 ]
        point [ x 2570.0 y 2500.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 211
    target 212
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 212
    target 210
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 214
    target 215
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 215
    target 213
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 90
    target 216
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2510.0 y 3410.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "first"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 91
    target 216
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2720.0 y 3410.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 121
    target 216
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3330.0 y 2950.0 ]
        point [ x 3310.0 y 3240.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 216
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 216
    target 212
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 219
    target 221
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 220
    target 221
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 221
    target 217
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 221
    target 218
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 223
    target 225
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 224
    target 225
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 225
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 226
    target 225
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 221
    target 226
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 100
    target 227
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 182
    target 227
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 227
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 227
    target 221
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 229
    target 230
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 230
    target 228
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 216
    target 230
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2730.0 y 3480.0 ]
        point [ x 2740.0 y 3510.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 230
    target 227
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2740.0 y 3650.0 ]
        point [ x 2730.0 y 3670.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 232
    target 235
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 234
    target 235
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 235
    target 231
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 235
    target 233
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 49
    target 236
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 236
    target 235
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 237
    target 239
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 238
    target 240
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 239
    target 238
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 242
    target 246
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 243
    target 246
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 246
    target 245
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 1466.0 y 864.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 246
    target 244
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 246
    target 241
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 246
    target 250
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 246
    target 249
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 246
    target 248
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 247
    target 246
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 240
    target 251
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 251
    target 246
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 253
    target 256
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 255
    target 256
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 256
    target 252
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 256
    target 254
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 257
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 257
    target 68
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 256
    target 257
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 215
    target 258
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 225
    target 259
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 260
    target 263
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 261
    target 263
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 263
    target 262
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 265
    target 264
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 266
    target 265
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 263
    target 266
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 212
    target 267
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 267
    target 215
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 267
    target 263
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 268
    target 273
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 270
    target 273
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 273
    target 272
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 273
    target 271
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 273
    target 269
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 275
    target 276
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 276
    target 274
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 277
    target 278
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 276
    target 277
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 272
    target 278
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 280
    target 281
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 281
    target 279
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 275
    target 281
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 283
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 284
    target 282
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 285
    target 289
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 288
    target 289
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 289
    target 287
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 289
    target 286
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 281
    target 283
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 284
    target 288
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 290
    target 278
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 291
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 281
    target 292
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 295
    target 294
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 295
    target 293
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 296
    target 297
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 299
    target 303
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 302
    target 303
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 303
    target 301
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 303
    target 300
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 303
    target 298
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 294
    target 297
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 297
    target 302
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 61
    target 295
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 305
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 306
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 307
    target 304
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 309
    target 311
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 310
    target 311
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 311
    target 308
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 313
    target 317
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 315
    target 317
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 316
    target 317
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 317
    target 314
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 317
    target 312
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 320
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 321
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 322
    target 319
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 322
    target 318
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 325
    target 326
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 326
    target 324
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 326
    target 323
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 328
    target 330
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 329
    target 330
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 330
    target 327
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 333
    target 332
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 333
    target 331
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 334
    target 337
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 337
    target 336
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 337
    target 335
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 339
    target 340
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 340
    target 338
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 341
    target 344
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 343
    target 344
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 344
    target 342
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 346
    target 350
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 347
    target 350
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 349
    target 350
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 350
    target 348
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 350
    target 345
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 306
    target 311
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 310
    target 317
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 316
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 321
    target 326
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 326
    target 329
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 330
    target 339
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 344
    target 349
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 348
    target 333
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 332
    target 337
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 181
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 1940.0 y 3900.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 353
    target 356
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 354
    target 356
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 356
    target 355
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 356
    target 351
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 356
    target 352
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 306
    target 356
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 356
    target 359
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 356
    target 358
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 357
    target 356
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 361
    target 364
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 363
    target 364
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 364
    target 362
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 364
    target 360
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 367
    target 369
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 368
    target 369
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 369
    target 365
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 369
    target 366
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 370
    target 373
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 372
    target 373
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 373
    target 371
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 369
    target 374
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 373
    target 362
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3201.7142857142853 y 4590.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 363
    target 373
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3201.7142857142853 y 4980.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 362
    target 369
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3540.0 y 4590.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 369
    target 363
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3540.0 y 4980.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 378
    target 380
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 375
    target 380
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 380
    target 377
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 380
    target 376
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 379
    target 380
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 146
    target 380
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 380
    target 362
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 146
    target 373
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3020.0 y 4860.0 ]
        point [ x 3090.0 y 4900.0 ]
        point [ x 3170.0 y 4880.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 9
    target 381
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 16
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 14
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 4
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 870.0 y 540.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 17
    target 382
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 382
    target 381
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 387
    target 388
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 388
    target 385
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 388
    target 386
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 381
    target 388
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 388
    target 383
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 390
    target 391
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 391
    target 389
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 394
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 396
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 397
    target 393
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 400
    target 404
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 401
    target 404
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 402
    target 404
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 404
    target 403
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 404
    target 398
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 404
    target 399
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 394
    target 391
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 391
    target 393
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 405
    target 406
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 385
    target 406
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 406
    target 387
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 406
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 610.0 y 1110.0 ]
        point [ x 600.0 y 1060.0 ]
        point [ x 600.0 y 780.0 ]
        point [ x 600.0 y 740.0 ]
        point [ x 630.0 y 740.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 7
    target 406
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 590.0 y 890.0 ]
        point [ x 570.0 y 900.0 ]
        point [ x 570.0 y 930.0 ]
        point [ x 570.0 y 1260.0 ]
        point [ x 590.0 y 1290.0 ]
        point [ x 630.0 y 1260.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 407
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 257
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 409
    target 413
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 410
    target 413
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 413
    target 412
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 413
    target 411
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 407
    target 413
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 417
    target 416
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 417
    target 415
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 417
    target 414
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 412
    target 417
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 418
    target 428
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 425
    target 428
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 426
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 428
    target 419
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 428
    target 426
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 429
    target 425
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 433
    target 434
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 434
    target 432
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 427
    target 428
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 428
    target 422
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 424
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 429
    target 423
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 423
    target 434
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 434
    target 424
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 420
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 429
    target 421
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 434
    target 430
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 431
    target 434
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 392
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 397
    target 395
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 435
    target 437
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 436
    target 437
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 437
    target 438
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 180
    target 437
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 437
    target 209
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 440
    target 450
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 442
    target 450
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 446
    target 449
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 448
    target 450
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 449
    target 447
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 450
    target 446
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 450
    target 439
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 450
    target 444
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 452
    target 459
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 455
    target 458
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 458
    target 457
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 458
    target 453
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 458
    target 456
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 459
    target 456
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 459
    target 451
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 459
    target 454
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 459
    target 339
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 460
    target 461
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 660.0 y 4640.0 ]
        point [ x 640.0 y 4640.0 ]
        point [ x 640.0 y 4620.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 340
    target 461
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 461
    target 344
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 447
    target 460
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 441
    target 460
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 460
    target 445
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 460
    target 443
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 383
    target 384
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 463
    target 464
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 464
    target 462
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 21
    target 464
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 464
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 468
    target 473
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 470
    target 473
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 471
    target 472
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 472
    target 466
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 472
    target 467
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 473
    target 469
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 473
    target 465
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 42
    target 472
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 472
    target 50
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 50
    target 473
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 473
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 384
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 130
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 131
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 132
    target 128
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 128
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 475
    target 477
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 477
    target 476
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 477
    target 474
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 216
    target 477
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2490.0 y 3480.0 ]
        point [ x 2480.0 y 3500.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 477
    target 227
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2480.0 y 3650.0 ]
        point [ x 2510.0 y 3670.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 96
    target 477
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 477
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 435
    target 173
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2040.0 y 2730.0 ]
        point [ x 2070.0 y 2740.0 ]
        point [ x 2060.0 y 2780.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 480
    target 482
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 482
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 482
    target 481
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 483
    target 478
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 483
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 181
    target 483
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 188
    target 482
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 482
    target 216
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2230.0 y 3460.0 ]
        point [ x 2300.0 y 3430.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 483
    target 216
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 2340.0 y 3540.0 ]
        point [ x 2370.0 y 3480.0 ]
        point [ x 2430.0 y 3450.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 484
    target 495
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 486
    target 496
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 488
    target 495
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 489
    target 497
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 493
    target 495
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 494
    target 496
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3290.0 y 4290.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 494
    target 497
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3650.0 y 4300.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 495
    target 494
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 495
    target 485
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 495
    target 490
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 495
    target 487
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 496
    target 491
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 496
    target 493
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3290.0 y 4020.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 497
    target 492
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 497
    target 493
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3650.0 y 4030.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 500
    target 511
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 501
    target 511
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 503
    target 510
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 504
    target 509
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 506
    target 511
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 507
    target 510
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 509
    target 498
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 509
    target 507
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 510
    target 505
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 510
    target 506
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 511
    target 508
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 511
    target 499
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 511
    target 502
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 111
    target 509
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 509
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 512
    target 514
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 514
    target 513
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 50
    target 514
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 750.0 y 3160.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 514
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 750.0 y 3340.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 516
    target 520
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 517
    target 520
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 519
    target 520
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 520
    target 518
    cluster [
      edgecount 1
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 520
    target 515
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 521
    target 519
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 50
    target 521
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 780.0 y 3110.0 ]
        point [ x 670.0 y 3120.0 ]
        point [ x 580.0 y 3180.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 520
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 590.0 y 3590.0 ]
        point [ x 680.0 y 3640.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 522
    target 524
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 524
    target 528
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 529
    target 535
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 532
    target 535
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 534
    target 535
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 535
    target 533
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 535
    target 530
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 535
    target 531
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 526
    target 524
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 524
    target 527
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 524
    target 523
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 525
    target 524
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 536
    target 540
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 539
    target 540
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "org.graffiti.plugins.views.defaults.PolyLineEdgeShape"
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 540
    target 537
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 540
    target 538
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 542
    target 545
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 545
    target 543
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 545
    target 541
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 265
    target 544
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 544
    target 545
    graphics [
      fill "#000000"
      outline "#000000"
      Line [
        point [ x 0.0 y 0.0 ]
        point [ x 3580.0 y 3360.0 ]
        point [ x 0.0 y 0.0 ]
      ]
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 544
    target 273
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 540
    target 258
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 182
    target 546
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
