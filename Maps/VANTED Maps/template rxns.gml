# generated with VANTED V2.1.0 at Tue Apr 30 13:45:03 PDT 2013
graph [
  directed 1
  node [
    id 1
    zlevel -1

    graphics [
      x 560.0
      y 190.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    graphics [
      x 620.0
      y 240.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "met"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    graphics [
      x 502.0
      y 239.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    graphics [
      x 561.0
      y 434.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    graphics [
      x 621.0
      y 384.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "met"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    graphics [
      x 502.0
      y 384.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    graphics [
      x 560.0
      y 312.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "RXN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    graphics [
      x 502.0
      y 764.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    graphics [
      x 502.0
      y 619.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    graphics [
      x 620.0
      y 620.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "met"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    graphics [
      x 621.0
      y 764.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "met"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    graphics [
      x 560.0
      y 570.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    graphics [
      x 561.0
      y 814.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    graphics [
      x 560.0
      y 692.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "RXN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    graphics [
      x 877.9285714285716
      y 370.78571428571445
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    graphics [
      x 878.9285714285714
      y 252.78571428571436
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    graphics [
      x 1022.9285714285716
      y 370.7857142857142
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    graphics [
      x 1022.9285714285714
      y 251.78571428571428
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    graphics [
      x 828.9285714285714
      y 312.7857142857142
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    graphics [
      x 1072.9285714285716
      y 311.78571428571433
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    graphics [
      x 951.4285714285714
      y 312.78571428571433
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "RXN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    graphics [
      x 1022.9285714285716
      y 631.7857142857142
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    graphics [
      x 878.9285714285714
      y 632.7857142857143
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "n"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    graphics [
      x 877.9285714285716
      y 750.7857142857144
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    graphics [
      x 1022.9285714285716
      y 750.7857142857142
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    graphics [
      x 828.9285714285714
      y 692.7857142857142
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    graphics [
      x 1072.9285714285716
      y 691.7857142857143
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br><br>"
    labelgraphics [
      alignment "center"
      anchor "s"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    graphics [
      x 951.4285714285714
      y 692.7857142857143
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "RXN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    graphics [
      x 212.0
      y 429.0
      w 25.0
      h 25.0
      fill "#FFFF99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    graphics [
      x 330.0
      y 430.0
      w 25.0
      h 25.0
      fill "#FFFF99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "met"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    graphics [
      x 212.0
      y 574.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "w"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    graphics [
      x 331.0
      y 574.0
      w 25.0
      h 25.0
      fill "#FFCC99"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "met"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    graphics [
      x 270.0
      y 380.0
      w 40.0
      h 25.0
      fill "#FFFF00"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    graphics [
      x 271.0
      y 624.0
      w 40.0
      h 25.0
      fill "#FF9900"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "circle"
    ]
    label "<html>met<br>"
    labelgraphics [
      alignment "center"
      anchor "e"
      color "#000000"
      fontName "Arial"
      fontSize 14
      fontStyle "plain"
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    graphics [
      x 270.0
      y 502.5
      w 60.0
      h 40.0
      fill "#CCCCFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      type "rectangle"
    ]
    label "RXN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 16
      fontStyle "bold,italic"
      type "text"
    ]
  ]
  edge [
    id 1
    source 1
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 2
    source 2
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 3
    source 7
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 4
    source 7
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 7
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 6
    source 3
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 9
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 8
    source 10
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 12
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 10
    source 14
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 11
    source 14
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 12
    source 14
    target 11
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 15
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 14
    source 16
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 19
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 21
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 21
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 21
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 23
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 24
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 26
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 28
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 28
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 28
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 29
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      smooth 1
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 30
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 33
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 35
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 2.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 35
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 35
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
