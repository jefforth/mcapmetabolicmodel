% Mcap model 1.13 general characterization:  do flux varaiblity analysis
% for key conditions
%
% Jeff Orth  9/28/17

changeCobraSolver('gurobi6','LP');

% model 1.13
load('M_capsulatus_v1.13b_rBN_model.mat');



% 1. max biomass from methane
model1 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');
[minFlux1_100,maxFlux1_100] = fluxVariability(model1,100,'max');
[minFlux1_90,maxFlux1_90] = fluxVariability(model1,90,'max');

% 2. max isobutanol from methane
model2 = changeObjective(model,'EX_ibutoh(e)');
model2 = changeRxnBounds(model2,{'3MOBDC','ALCD23yr'},1000,'u');
[minFlux2_100,maxFlux2_100] = fluxVariability(model2,100,'max');
[minFlux2_90,maxFlux2_90] = fluxVariability(model2,90,'max');

% 3. max isobutanol from methane and 4% ethane, final production protocol
model3 = changeObjective(model,'EX_ibutoh(e)');
model3 = changeRxnBounds(model3,{'3MOBDC','ALCD23yr'},1000,'u');
model3 = changeRxnBounds(model3,'EX_ch4(e)',-19.2,'l'); % 96% CH4
model3 = changeRxnBounds(model3,'EX_etn(e)',-0.8,'l'); % 4% ethane
model3 = changeRxnBounds(model3,'Biomass_Mcapsulatus_v4_100p0M',0.005,'l');
[minFlux3_100,maxFlux3_100] = fluxVariability(model3,100,'max');
[minFlux3_90,maxFlux3_90] = fluxVariability(model3,90,'max');


