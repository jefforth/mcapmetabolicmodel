% Mcap model 1.13 general characterization:  detailed test of model
% simulations with non-default fluxes for simulating specific phenotypes
%
% Jeff Orth  9/27/17

changeCobraSolver('gurobi6','LP');

load('M_capsulatus_v1.13b_rBN_model.mat');


% 1. WT (pMMO, RuMP, aerobic, no heterologous pathways)
% max biomass
model1 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');
fbasol1 = optimizeCbModel(model1,'max','one');
fprintf('1. WT \n');
printFluxVector(model1,fbasol1.x,true,true);
fprintf('\n\n');


% 2. low Cu (sMMO)
% max biomass
model2 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');
model2 = changeRxnBounds(model2,'ETNPMMOpp',0,'u');
model2 = changeRxnBounds(model2,'ETNSMMO',1000,'u');
model2 = changeRxnBounds(model2,'PMMOpp',0,'b');
model2 = changeRxnBounds(model2,'SMMO',1000,'u');
fbasol2 = optimizeCbModel(model2,'max','one');
fprintf('2. sMMO \n');
printFluxVector(model2,fbasol2.x,true,true);
fprintf('\n\n');


% 3. high La (xoxFJ)
% max biomass
model3 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');
model3 = changeRxnBounds(model3,'MEOHDH5',1000,'u');
fbasol3 = optimizeCbModel(model3,'max','one');
fprintf('3. xoxFJ \n');
printFluxVector(model3,fbasol3.x,true,true);
fprintf('\n\n');


% 4. microaerobic
% max biomass
model4 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');
model4 = changeRxnBounds(model4,'CYOO2pp',0,'b');
model4 = changeRxnBounds(model4,'CYTCHO2',5,'u');
model4 = changeRxnBounds(model4,'GLYO1',0,'b');
model4 = changeRxnBounds(model4,'HDAAOR4',0,'u');
model4 = changeRxnBounds(model4,'ODAAOR4',0,'u');
model4 = changeRxnBounds(model4,'EX_meoh(e)',0,'u');
fbasol4 = optimizeCbModel(model4,'max','one');
fprintf('4. microaerobic \n');
printFluxVector(model4,fbasol4.x,true,true);
fprintf('\n\n');


% 5. ethane
% max biomass
model5 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');
model5 = changeRxnBounds(model5,'ALDD2q',1000,'u');
model5 = changeRxnBounds(model5,'POR5',-1000,'l');
model5 = changeRxnBounds(model5,'ME1',0,'u');
model5 = changeRxnBounds(model5,'OAADCr',0,'u');
model5 = changeRxnBounds(model5,'EX_ch4(e)',0,'l');
model5 = changeRxnBounds(model5,'EX_etn(e)',-20,'l');
fbasol5 = optimizeCbModel(model5,'max','one');
fprintf('5. ethane \n');
printFluxVector(model5,fbasol5.x,true,true);
fprintf('\n\n');


% 6. high NH4+ nitrogen source
% max biomass
model6 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');
model6 = changeRxnBounds(model6,'GLUDy',-1000,'l');
model6 = changeRxnBounds(model6,'GLUDy',1000,'u');
model6 = changeRxnBounds(model6,'GLUN',1000,'u');
model6 = changeRxnBounds(model6,'GLUSy',0,'u');
model6 = changeRxnBounds(model6,'NH3PMMOpp',1000,'u');
fbasol6 = optimizeCbModel(model6,'max','one');
fprintf('6. high NH4+ \n');
printFluxVector(model6,fbasol6.x,true,true);
fprintf('\n\n');


% 7. serine pathway
% max biomass
model7 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');
model7 = changeRxnBounds(model7,'6P3HI',0,'u');
model7 = changeRxnBounds(model7,'POR5',-1000,'l');
fbasol7 = optimizeCbModel(model7,'max','one');
fprintf('7. serine pathway \n');
printFluxVector(model7,fbasol7.x,true,true);
fprintf('\n\n');


% 8. isobutanol (NADPH)
% max isobutanol
model8 = changeObjective(model,'EX_ibutoh(e)');
model8 = changeRxnBounds(model8,'3MOBDC',1000,'u');
model8 = changeRxnBounds(model8,'4MOPDCi',1000,'u');
model8 = changeRxnBounds(model8,'ALCD23yr',1000,'u');
model8 = changeRxnBounds(model8,'ALCD23yr',-1000,'l');
model8 = changeRxnBounds(model8,'ALCD24yr',1000,'u');
model8 = changeRxnBounds(model8,'ALCD24yr',-1000,'l');
model8 = changeRxnBounds(model8,'ALCD26xr',1000,'u');
model8 = changeRxnBounds(model8,'ALCD26xr',-1000,'l');
model8 = changeRxnBounds(model8,'ALCD27xr',1000,'u');
model8 = changeRxnBounds(model8,'ALCD27xr',-1000,'l');
fbasol8 = optimizeCbModel(model8,'max','one');
fprintf('8. isobutanol \n');
printFluxVector(model8,fbasol8.x,true,true);
fprintf('\n\n');


% 9. 23BDO (NADPH)
% max 23BDO
model9 = changeObjective(model,'EX_23btd-RR(e)');
model9 = changeRxnBounds(model9,'DIACR',1000,'u');
model9 = changeRxnBounds(model9,'DIACRy',1000,'u');
model9 = changeRxnBounds(model9,'DIACS',1000,'u');
model9 = changeRxnBounds(model9,'DIACSy',1000,'u');
model9 = changeRxnBounds(model9,'RBTDDHy',1000,'u');
model9 = changeRxnBounds(model9,'RBTDDHy',-1000,'l');
model9 = changeRxnBounds(model9,'RSBTDDHRy',1000,'u');
model9 = changeRxnBounds(model9,'RSBTDDHRy',-1000,'l');
model9 = changeRxnBounds(model9,'RSBTDDHSy',1000,'u');
model9 = changeRxnBounds(model9,'RSBTDDHSy',-1000,'l');
model9 = changeRxnBounds(model9,'SBTDDHy',1000,'u');
model9 = changeRxnBounds(model9,'SBTDDHy',-1000,'l');
fbasol9 = optimizeCbModel(model9,'max','one');
fprintf('9. 23BDO \n');
printFluxVector(model9,fbasol9.x,true,true);
fprintf('\n\n');


% 10. 14BDO pathway 1 (from AKG, NADPH)
% max 14BDO
model10 = changeObjective(model,'EX_14bdo(e)');
model10 = changeRxnBounds(model10,'GHBDHx',1000,'u');
model10 = changeRxnBounds(model10,'HBUTCRy',1000,'u');
model10 = changeRxnBounds(model10,'HBUTCT2',1000,'u');
model10 = changeRxnBounds(model10,'HBUTDHy',1000,'u');
model10 = changeRxnBounds(model10,'SUCSALDH',1000,'u');
fbasol10 = optimizeCbModel(model10,'max','one');
fprintf('10. 14BDO pathway 1 \n');
printFluxVector(model10,fbasol10.x,true,true);
fprintf('\n\n');

% 11. 14BDO pathway 2 (from succoa, NADPH)
% max 14BDO
model11 = changeObjective(model,'EX_14bdo(e)');
model11 = changeRxnBounds(model11,'GHBDHx',1000,'u');
model11 = changeRxnBounds(model11,'HBUTCRy',1000,'u');
model11 = changeRxnBounds(model11,'HBUTCT2',1000,'u');
model11 = changeRxnBounds(model11,'HBUTDHy',1000,'u');
model11 = changeRxnBounds(model11,'OXGDC',1000,'u');
fbasol11 = optimizeCbModel(model11,'max','one');
fprintf('11. 14BDO pathway 2 \n');
printFluxVector(model11,fbasol11.x,true,true);
fprintf('\n\n');




