% Mcap model 1.13 general characterization:  do gene/reaction essentiality 
% analysis for key conditions
%
% Jeff Orth  9/28/17

changeCobraSolver('gurobi6','LP');

% model 1.13
load('M_capsulatus_v1.13b_rBN_model.mat');

genes = sort(model.genes);



% 1. max biomass from methane
model1 = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');

model1_rxns = zeros(length(model1.rxns),1);
for i = 1:length(model1.rxns)
    model1_rxn = changeRxnBounds(model1,model1.rxns{i},0,'b');
    fbasol = optimizeCbModel(model1_rxn);
    if ~isempty(fbasol.x)
        model1_rxns(i) = fbasol.f;
    end
end

model1_genes = zeros(length(genes),1);
for i = 1:length(genes)
    model1_gene = deleteModelGenes(model1,genes{i});
    fbasol = optimizeCbModel(model1_gene);
    if ~isempty(fbasol.x)
        model1_genes(i) = fbasol.f;
    end
end



% 2. max isobutanol from methane
model2 = changeObjective(model,'EX_ibutoh(e)');
model2 = changeRxnBounds(model2,{'3MOBDC','ALCD23yr'},1000,'u');

model2_rxns = zeros(length(model2.rxns),1);
for i = 1:length(model2.rxns)
    model2_rxn = changeRxnBounds(model2,model2.rxns{i},0,'b');
    fbasol = optimizeCbModel(model2_rxn);
    if ~isempty(fbasol.x)
        model2_rxns(i) = fbasol.f;
    end
end

model2_genes = zeros(length(genes),1);
for i = 1:length(genes)
    model2_gene = deleteModelGenes(model2,genes{i});
    fbasol = optimizeCbModel(model2_gene);
    if ~isempty(fbasol.x)
        model2_genes(i) = fbasol.f;
    end
end



% 3. max isobutanol from methane and 4% ethane, final production protocol
model3 = changeObjective(model,'EX_ibutoh(e)');
model3 = changeRxnBounds(model3,{'3MOBDC','ALCD23yr'},1000,'u');
model3 = changeRxnBounds(model3,'EX_ch4(e)',-19.2,'l'); % 96% CH4
model3 = changeRxnBounds(model3,'EX_etn(e)',-0.8,'l'); % 4% ethane
model3 = changeRxnBounds(model3,'Biomass_Mcapsulatus_v4_100p0M',0.005,'l');

model3_rxns = zeros(length(model3.rxns),1);
for i = 1:length(model3.rxns)
    model3_rxn = changeRxnBounds(model3,model3.rxns{i},0,'b');
    fbasol = optimizeCbModel(model3_rxn);
    if ~isempty(fbasol.x)
        model3_rxns(i) = fbasol.f;
    end
end

model3_genes = zeros(length(genes),1);
for i = 1:length(genes)
    model3_gene = deleteModelGenes(model3,genes{i});
    fbasol = optimizeCbModel(model3_gene);
    if ~isempty(fbasol.x)
        model3_genes(i) = fbasol.f;
    end
end


