% Mcap model 1.13 general characterization:  run pFBA on the model,
% determine fluxes and categorize genes.  Note that gurobi stalls in the
% reduceModel step, ran for 2 days with no progress before I stopped it.
% 
% Jeff Orth  9/28/17

changeCobraSolver('gurobi6','LP');

% model 1.13
load('M_capsulatus_v1.13b_rBN_model.mat');



% 1. max biomass
model = changeObjective(model,'Biomass_Mcapsulatus_v4_100p0M');

% run pFBA
[GeneClasses,RxnClasses,modelIrrevFM] = pFBA(model);

% save full output
save('pFBA_biomass_gurobi','GeneClasses','RxnClasses','modelIrrevFM');



% 2. max isobutanol
model_iboh = changeObjective(model,'EX_ibutoh(e)');
model_iboh = changeRxnBounds(model_iboh,{'3MOBDC','ALCD23yr'},1000,'u');

% run pFBA
[GeneClasses_iboh,RxnClasses_iboh,modelIrrevFM_iboh] = pFBA(model_iboh);

% save full output
save('pFBA_iboh_gurobi','GeneClasses_iboh','RxnClasses_iboh','modelIrrevFM_iboh');




