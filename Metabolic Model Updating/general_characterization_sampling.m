% Mcap model 1.13 general characterization:  do random sampling of full
% solution space, with and without isobutanol production allowed.
% 
% Jeff Orth  9/28/17

changeCobraSolver('gurobi6','LP');

% model 1.13
load('M_capsulatus_v1.13b_rBN_model.mat');



% 1. no isobutanol production
% model = changeObjective(model,'Biomass_Mcapsulatus_v3_100p0M');
sampleStruct = model;
[sampleStructOut, mixedFraction] = gpSampler(sampleStruct);%, nPoints, bias, maxTime, maxSteps)

% remove numerical error from 0 fluxes (glpk)
corrected_points = sampleStructOut.points;
corrected_points(abs(corrected_points) < 10e-10) = 0;

% compute average and stdev
avg_fluxes = mean(corrected_points,2);
std_fluxes = std(corrected_points,0,2);
min_fluxes = min(corrected_points,[],2);
max_fluxes = max(corrected_points,[],2);

% plot key reactions
figure(1);
sampleScatterMatrix({'PMMOpp','MEOHDH4','AH6PFL','FDH','PYK','EDA','ACLS','PDH','CS','OAADCr'},model,corrected_points)

% save full output
save('random_sampling_no_iboh','sampleStructOut');



% 2. with isobutanol production
model_iboh = changeRxnBounds(model,{'3MOBDC','ALCD23yr'},1000,'u');
sampleStruct_iboh = model_iboh;
[sampleStructOut_iboh, mixedFraction_iboh] = gpSampler(sampleStruct_iboh);%, nPoints, bias, maxTime, maxSteps)

% remove numerical error from 0 fluxes (glpk)
corrected_points_iboh = sampleStructOut_iboh.points;
corrected_points_iboh(abs(corrected_points_iboh) < 10e-10) = 0;

% compute average and stdev
avg_fluxes_iboh = mean(corrected_points_iboh,2);
std_fluxes_iboh = std(corrected_points_iboh,0,2);
min_fluxes_iboh = min(corrected_points_iboh,[],2);
max_fluxes_iboh = max(corrected_points_iboh,[],2);

% plot key reactions
figure(2);
sampleScatterMatrix({'PMMOpp','MEOHDH4','AH6PFL','FDH','PYK','EDA','ACLS','PDH','CS','OAADCr'},model_iboh,corrected_points_iboh)

% save full output
save('random_sampling_with_iboh','sampleStructOut_iboh');




