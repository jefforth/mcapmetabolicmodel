% Mcap model 1.13 general characterization:  analyze yields of key products
% from key substrates under different conditions
%
% Jeff Orth  9/27/17

changeCobraSolver('gurobi6','LP');

% load model 1.13
load('M_capsulatus_v1.13b_rBN_model.mat');


% Substrates:
% 1. methane (C1)
% 2. methanol (C1)
% 3. ethane (C2)
% 4. acetate (C2)
% 5. pyruvate (c3)

% Products:
% 1. biomass (yield = gDW/mmol)
% 2. ATP
% 3. NADH
% 4. NADPH
% 5. acetate
% 6. lactate
% 7. pyruvate
% 8. glycogen
% 9. isobutanol
% 10. isopentaol
% 11. isobutyraldehyde
% 12. isovaleraldehyde
% 13. 23BDO
% 14. 14BDO (pathway 1)
% 15. 14BDO (pathway 2)

% Alternate conditions:
% 1. WT (pMMO, RuMP, aerobic, low NH4+)
% 2. low Cu (sMMO)
% 3. high La (xoxF)
% 4. serine pathway
% 5. microaerobic
% 6. high NH4+ nitrogen source
% 7. NO3 nitrogen source
% 8. N2 nitrogen source
% 9. MMO-MDH redox coupling unlimited

yields = zeros(5,15,9);
for i = 1:5
    for j = 1:15
        for k = 1:9
            model_i = model;
            
            % Substrates:
            % 1. methane (C1)
            if i == 1
                model_i = changeRxnBounds(model_i,'EX_ch4(e)',-20,'l');
                substrate = 'EX_ch4(e)';
                
            % 2. methanol (C1)
            elseif i == 2
                model_i = changeRxnBounds(model_i,'EX_ch4(e)',0,'l');
                model_i = changeRxnBounds(model_i,'EX_meoh(e)',-20,'b');
                substrate = 'EX_meoh(e)';
                
            % 3. ethane (C2)
            elseif i == 3
                model_i = changeRxnBounds(model_i,'EX_ch4(e)',0,'l');
                model_i = changeRxnBounds(model_i,'EX_etn(e)',-20/2,'b');
                model_i = changeRxnBounds(model_i,'CYTCLQ8',5,'u');
                model_i = changeRxnBounds(model_i,'ALDD2q',1000,'u');
                model_i = changeRxnBounds(model_i,'ME1',0,'u');
                model_i = changeRxnBounds(model_i,'OAADCr',0,'u');
                model_i = changeRxnBounds(model_i,'POR5',-1000,'l');
                substrate = 'EX_etn(e)';
                
            % 4. acetate (C2)
            elseif i == 4
                model_i = changeRxnBounds(model_i,'EX_ch4(e)',0,'l');
                model_i = changeRxnBounds(model_i,'EX_ac(e)',-20/2,'b');
                substrate = 'EX_ac(e)';
                
            % 5. pyruvate (C3)
            else % i == 5
                model_i = changeRxnBounds(model_i,'EX_ch4(e)',0,'l');
                model_i = changeRxnBounds(model_i,'EX_pyr(e)',-20/3,'b');
                substrate = 'EX_pyr(e)';
            end
            
            
            
            % Products:
            % 1. biomass (yield = gDW/mmol)
            if j == 1
                model_i = changeObjective(model_i,'Biomass_Mcapsulatus_v4_100p0M');
            
            % 2. ATP
            elseif j == 2
                model_i = changeObjective(model_i,'ATPM');
                model_i = changeRxnBounds(model_i,'ATPM',0,'l');
            
            % 3. NADH
            elseif j == 3
                model_i = addReaction(model_i,'DM_NADH','nadh[c] -> nad[c] + h[c]');
                model_i = changeObjective(model_i,'DM_NADH');
                model_i = changeRxnBounds(model_i,'ATPM',0,'l');
            
            % 4. NADPH
            elseif j == 4
                model_i = addReaction(model_i,'DM_NADPH','nadph[c] -> nadp[c] + h[c]');
                model_i = changeObjective(model_i,'DM_NADPH');
                model_i = changeRxnBounds(model_i,'ATPM',0,'l');
            
            % 5. acetate
            elseif j == 5
                model_i = changeObjective(model_i,'EX_ac(e)');
            
            % 6. lactate
            elseif j == 6
                model_i = changeObjective(model_i,'EX_lac-D(e)');
            
            % 7. pyruvate
            elseif j == 7
                model_i = changeObjective(model_i,'EX_pyr(e)');
            
            % 8. glycogen
            elseif j == 8
                model_i = changeObjective(model_i,'DM_glycogen(c)');
            
            % 9. isobutanol
            elseif j == 9
                model_i = changeObjective(model_i,'EX_ibutoh(e)');
                model_i = changeRxnBounds(model_i,'3MOBDC',1000,'u');
                model_i = changeRxnBounds(model_i,'ALCD23yr',1000,'u');
                model_i = changeRxnBounds(model_i,'ALCD23yr',-1000,'l');
            
            % 10. isopentanol
            elseif j == 10
                model_i = changeObjective(model_i,'EX_iamoh(e)');
                model_i = changeRxnBounds(model_i,'4MOPDCi',1000,'u');
                model_i = changeRxnBounds(model_i,'ALCD24yr',1000,'u');
                model_i = changeRxnBounds(model_i,'ALCD24yr',-1000,'l');
            
            % 11. isobutyraldehyde
            elseif j == 11
                model_i = changeObjective(model_i,'EX_2mppal(e)');
                model_i = changeRxnBounds(model_i,'3MOBDC',1000,'u');
                model_i = changeRxnBounds(model_i,'ALCD23yr',1000,'u');
                model_i = changeRxnBounds(model_i,'ALCD23yr',-1000,'l');
            
            % 12. isovaleraldehyde
            elseif j == 12
                 model_i = changeObjective(model_i,'EX_3mbald(e)');
                 model_i = changeRxnBounds(model_i,'4MOPDCi',1000,'u');
                 model_i = changeRxnBounds(model_i,'ALCD24yr',1000,'u');
                 model_i = changeRxnBounds(model_i,'ALCD24yr',-1000,'l');
            
            % 13. 23BDO
            elseif j == 13
                model_i = changeObjective(model_i,'EX_23btd-RR(e)');
                model_i = changeRxnBounds(model_i,'DIACR',1000,'u');
                model_i = changeRxnBounds(model_i,'DIACRy',1000,'u');
                model_i = changeRxnBounds(model_i,'DIACS',1000,'u');
                model_i = changeRxnBounds(model_i,'DIACSy',1000,'u');
                model_i = changeRxnBounds(model_i,'RBTDDHy',1000,'u');
                model_i = changeRxnBounds(model_i,'RBTDDHy',-1000,'l');
            
            % 14. 14BDO (pathway 1)
            elseif j == 14
                model_i = changeObjective(model_i,'EX_14bdo(e)');
                model_i = changeRxnBounds(model_i,'GHBDHx',1000,'u');
                model_i = changeRxnBounds(model_i,'HBUTCRy',1000,'u');
                model_i = changeRxnBounds(model_i,'HBUTCT2',1000,'u');
                model_i = changeRxnBounds(model_i,'HBUTDHy',1000,'u');
                model_i = changeRxnBounds(model_i,'SUCSALDH',1000,'u');
            
            % 15. 14BDO (pathway 2)
            else % j == 15
                model_i = changeObjective(model_i,'EX_14bdo(e)');
                model_i = changeRxnBounds(model_i,'GHBDHx',1000,'u');
                model_i = changeRxnBounds(model_i,'HBUTCRy',1000,'u');
                model_i = changeRxnBounds(model_i,'HBUTCT2',1000,'u');
                model_i = changeRxnBounds(model_i,'HBUTDHy',1000,'u');
                model_i = changeRxnBounds(model_i,'OXGDC',1000,'u');
            end
            
            
            
            % Alternate conditions:
            % 1. WT (pMMO, RuMP, aerobic, low NH4+)
            if k == 1
                % skip, no changes needed
            
            % 2. low Cu (sMMO)
            elseif k == 2
                model_i = changeRxnBounds(model_i,'ETNPMMOpp',0,'u');
                model_i = changeRxnBounds(model_i,'ETNSMMO',1000,'u');
                model_i = changeRxnBounds(model_i,'PMMOpp',0,'b');
                model_i = changeRxnBounds(model_i,'SMMO',1000,'u');
            
            % 3. high La (xoxF)
            elseif k == 3
                model_i = changeRxnBounds(model_i,'MEOHDH5',1000,'u');
            
            % 4. serine pathway
            elseif k == 4
                model_i = changeRxnBounds(model_i,'6P3HI',0,'u');
                model_i = changeRxnBounds(model_i,'POR5',-1000,'l');
            
            % 5. microaerobic
            elseif k == 5
                model_i = changeRxnBounds(model_i,'CYOO2pp',0,'b');
                model_i = changeRxnBounds(model_i,'CYTCHO2',5,'u');
                model_i = changeRxnBounds(model_i,'GLYO1',0,'b');
                model_i = changeRxnBounds(model_i,'HDAAOR4',0,'u');
                model_i = changeRxnBounds(model_i,'ODAAOR4',0,'u');
                model_i = changeRxnBounds(model_i,'EX_meoh(e)',0,'u');
            
            % 6. high NH4+ nitrogen source
            elseif k == 6
                model_i = changeRxnBounds(model_i,'GLUDy',-1000,'l');
                model_i = changeRxnBounds(model_i,'GLUDy',1000,'u');
                model_i = changeRxnBounds(model_i,'GLUN',1000,'u');
                model_i = changeRxnBounds(model_i,'GLUSy',0,'u');
                model_i = changeRxnBounds(model_i,'NH3PMMOpp',1000,'u');
            
            % 7. NO3 nitrogen source
            elseif k == 7
                model_i = changeRxnBounds(model_i,'EX_no3(e)',-1000,'l');
                model_i = changeRxnBounds(model_i,'EX_nh4(e)',0,'l');
            
            % 8. N2 nitrogen source
            elseif k == 8
                model_i = changeRxnBounds(model_i,'EX_n2(e)',-1000,'l');
                model_i = changeRxnBounds(model_i,'EX_nh4(e)',0,'l');
            
            % 9. MMO-MDH redox coupling unlimited
            elseif k == 9
                model_i = changeRxnBounds(model_i,'CYTCLQ8',1000,'u');    
            end
            
            
            
            % max yield by FBA
            if ~((i == 4)&&(j == 5)) && ~((i == 5)&&(j == 7)) % skip acetate => acetate and pyruvate => pyruvate
                fbasol = optimizeCbModel(model_i,'max','one');
                if ~isempty(fbasol.x)
                    yield = fbasol.f / -fbasol.x(findRxnIDs(model,substrate));
                    yields(i,j,k) = yield;
                end
            end
        end
    end
end


