% run GapFind on Mcap model 1.12mets
%
% Jeff Orth  9/12/17

changeCobraSolver('gurobi5','MILP');



% load model
load('M_capsulatus_v1.12mets_rBN_model.mat');

% allow uptake of all substrates
modelGF = changeRxnBounds(model,{model.rxns{447:549}},-1000,'l');

% increase UB on all outputs
modelGF = changeRxnBounds(modelGF,{modelGF.rxns{:}},1000000000,'u');

% remove other constraints
modelGF = changeRxnBounds(modelGF,'ATPM',0,'l');



% run MILP
%find gaps
[allGapsNP,rootGapsNP,downstreamGapsNP] = gapFind(modelGF,false);
[allGapsNPNC,rootGapsNPNC,downstreamGapsNPNC] = gapFind(modelGF,true);
rootGapsNC = setdiff(rootGapsNPNC,rootGapsNP);
downstreamGapsNC = setdiff(downstreamGapsNPNC,downstreamGapsNP);