% run the standard test function on new Mcap model 1.13, compare results to
% model 1.12
%
% Jeff Orth  9/19/17

changeCobraSolver('gurobi6','LP');



% model 1.12
% load('M_capsulatus_v1.12mets_rBN_model.mat');
% results12 = standard_tests_Mcap_model(model);

% model 1.13a
% load('M_capsulatus_v1.13a_rBN_model.mat');
% model = changeRxnBounds(model,{'ALCD2x','ALCD2y'},0,'l'); % irreversible to avoid transhydrogenase flux loops
% model = changeRxnBounds(model,'THD2rpp',0,'l'); % irreversible to avoid flux loops
% model = changeRxnBounds(model,'PFL',-1000,'l'); % needed for serine cycle CH4 assimilation
% model = changeRxnBounds(model,{'POR5','FLDR2'},-1000,'l'); % needed for serine cycle CH4 assimilation
% results13a = standard_tests_Mcap_model(model);

% model 1.13b
load('M_capsulatus_v1.13c_rBN_model.mat');
model = changeRxnBounds(model,'POR5',-1000,'l'); % needed for serine cycle CH4 assimilation
% model = changeRxnBounds(model,'FMFDH',-1000,'l'); % test irreversible reaction
results13c = standard_tests_Mcap_model(model);



%%%
% tests passed, updated model produces same result as model 1.12
%%%


