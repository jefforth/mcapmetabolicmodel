% results = standard_tests_Mcap_model(model)
%
% Run a standard set of tests on an M. capsulatus metabolic model.  Use to
% validate new model versions.
%
% Tests:
%
% 1. max ATP from 0 C inputs
% 2. max ATP from methane, aerobic
% 3. max ATP from methane, microaerobic
% 4. max biomass from 0 C inputs
% 5. max biomass from methane, aerobic
% 6. max biomass from methane, microaerobic
% 7. max biomass from methane, RuMP pathway
% 8. max biomass from methane, serine pathway
% 9. max biomass from methane, pMMO
% 10. max biomass from methane, sMMO
% 11. max biomass from methane, N2
% 12. max isobutanol from methane, aerobic
% 13. max isobutanol from methane, microaerobic
% 14. max isobutyraldehyde from methane, aerobic
% 15. max isobutyraldehyde from methane, microaerobic
% 16. max 23BDO from methane, aerobic
% 17. max 23BDO from methane, microaerobic
% 18. max 14BDO from methane, aerobic
% 19. max 14BDO from methane, microaerobic
% 20. max acetate from methane, aerobic
% 21. max acetate from methane, microaerobic
% 22. max lactate from methane, aerobic
% 23. max lactate from methane, microaerobic
% 24. max pyruvate from methane, aerobic
% 25. max pyruvate from methane, microaerobic
% 26. max glycogen from methane, aerobic
% 27. max glycogen from methane, microaerobic
% 28. max ATP from methanol
% 29. max ATP from ethane
% 30. max biomass from methanol
% 31. max biomass from ethane
%
% Jeff Orth  9/19/17

function results = standard_tests_Mcap_model(model)

% relax some default constraints
model = changeRxnBounds(model,'ATPM',0,'l');
model = changeRxnBounds(model,'CYTCLQ8',1000,'u');
model = changeRxnBounds(model,'EX_ch4(e)',0,'l');
model = changeRxnBounds(model,'EX_n2(e)',0,'l');
model = changeRxnBounds(model,'EX_o2(e)',-1000,'l');



% 1. max ATP from 0 C inputs
model1 = changeRxnBounds(model,'EX_ch4(e)',0,'l');
model1 = changeObjective(model1,'ATPM');
fbasol1 = optimizeCbModel(model1);

% 2. max ATP from methane, aerobic
model2 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model2 = changeObjective(model2,'ATPM');
fbasol2 = optimizeCbModel(model2);

% 3. max ATP from methane, microaerobic
model3 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model3 = changeRxnBounds(model3,'EX_meoh(e)',0,'u');
model3 = changeRxnBounds(model3,'CYTCLQ8',0,'l');
model3 = changeRxnBounds(model3,'CYTCLQ8',10*0.5,'u');
model3 = changeRxnBounds(model3,'CYOO2pp',0,'b');
model3 = changeRxnBounds(model3,'CYTCHO2',10*0.25,'u');
model3 = changeRxnBounds(model3,'GLYO1',0,'b');
model3 = changeRxnBounds(model3,'HDAAOR4',0,'u');
model3 = changeRxnBounds(model3,'ODAAOR4',0,'u');
model3 = changeObjective(model3,'ATPM');
fbasol3 = optimizeCbModel(model3);

% 4. max biomass from 0 C inputs
model4 = changeRxnBounds(model,'EX_ch4(e)',0,'l');
if findRxnIDs(model4,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model4 = changeObjective(model4,'Biomass_Mcapsulatus_v4_100p0M');
else
    model4 = changeObjective(model4,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol4 = optimizeCbModel(model4,'max','one');

% 5. max biomass from methane, aerobic
model5 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
if findRxnIDs(model5,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model5 = changeObjective(model5,'Biomass_Mcapsulatus_v4_100p0M');
else
    model5 = changeObjective(model5,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol5 = optimizeCbModel(model5,'max','one');

% 6. max biomass from methane, microaerobic
model6 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model6 = changeRxnBounds(model6,'EX_meoh(e)',0,'u');
model6 = changeRxnBounds(model6,'CYTCLQ8',0,'l');
model6 = changeRxnBounds(model6,'CYTCLQ8',10*0.5,'u');
model6 = changeRxnBounds(model6,'CYOO2pp',0,'b');
model6 = changeRxnBounds(model6,'CYTCHO2',10*0.25,'u');
model6 = changeRxnBounds(model6,'GLYO1',0,'b');
model6 = changeRxnBounds(model6,'HDAAOR4',0,'u');
model6 = changeRxnBounds(model6,'ODAAOR4',0,'u');
if findRxnIDs(model6,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model6 = changeObjective(model6,'Biomass_Mcapsulatus_v4_100p0M');
else
    model6 = changeObjective(model6,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol6 = optimizeCbModel(model6);

% 7. max biomass from methane, RuMP pathway
model7 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model7 = changeRxnBounds(model7,{'FTHFLi'},0,'b');
if findRxnIDs(model7,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model7 = changeObjective(model7,'Biomass_Mcapsulatus_v4_100p0M');
else
    model7 = changeObjective(model7,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol7 = optimizeCbModel(model7);

% 8. max biomass from methane, serine pathway
model8 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model8 = changeRxnBounds(model8,{'6P3HI'},0,'b');
if findRxnIDs(model8,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model8 = changeObjective(model8,'Biomass_Mcapsulatus_v4_100p0M');
else
    model8 = changeObjective(model8,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol8 = optimizeCbModel(model8);

% 9. max biomass from methane, pMMO
model9 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model9 = changeRxnBounds(model9,{'SMMO'},0,'b');
if findRxnIDs(model9,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model9 = changeObjective(model9,'Biomass_Mcapsulatus_v4_100p0M');
else
    model9 = changeObjective(model9,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol9 = optimizeCbModel(model9);

% 10. max biomass from methane, sMMO
model10 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model10 = changeRxnBounds(model10,{'PMMOpp'},0,'b');
model10 = changeRxnBounds(model10,{'SMMO'},1000,'u');
if findRxnIDs(model10,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model10 = changeObjective(model10,'Biomass_Mcapsulatus_v4_100p0M');
else
    model10 = changeObjective(model10,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol10 = optimizeCbModel(model10);

% 11. max biomass from methane, N2
model11 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model11 = changeRxnBounds(model11,'EX_nh4(e)',0,'l');
model11 = changeRxnBounds(model11,'EX_n2(e)',-1000,'l');
if findRxnIDs(model11,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model11 = changeObjective(model11,'Biomass_Mcapsulatus_v4_100p0M');
else
    model11 = changeObjective(model11,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol11 = optimizeCbModel(model11,'max','one');

% 12. max isobutanol from methane, aerobic
model12 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model12 = changeRxnBounds(model12,{'3MOBDC','ALCD23yr'},1000,'u');
model12 = changeObjective(model12,'EX_ibutoh(e)');
fbasol12 = optimizeCbModel(model12);

% 13. max isobutanol from methane, microaerobic
model13 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model13 = changeRxnBounds(model13,'EX_meoh(e)',0,'u');
model13 = changeRxnBounds(model13,'CYTCLQ8',0,'l');
model13 = changeRxnBounds(model13,'CYTCLQ8',10*0.5,'u');
model13 = changeRxnBounds(model13,'CYOO2pp',0,'b');
model13 = changeRxnBounds(model13,'CYTCHO2',10*0.25,'u');
model13 = changeRxnBounds(model13,'GLYO1',0,'b');
model13 = changeRxnBounds(model13,{'3MOBDC','ALCD23yr'},1000,'u');
model13 = changeRxnBounds(model13,'HDAAOR4',0,'u');
model13 = changeRxnBounds(model13,'ODAAOR4',0,'u');
model13 = changeObjective(model13,'EX_ibutoh(e)');
fbasol13 = optimizeCbModel(model13);

% 14. max isobutyraldehyde from methane, aerobic
model14 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model14 = changeRxnBounds(model14,'3MOBDC',1000,'u');
model14 = changeObjective(model14,'EX_2mppal(e)');
fbasol14 = optimizeCbModel(model14);

% 15. max isobutyraldehyde from methane, microaerobic
model15 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model15 = changeRxnBounds(model15,'EX_meoh(e)',0,'u');
model15 = changeRxnBounds(model15,'CYTCLQ8',0,'l');
model15 = changeRxnBounds(model15,'CYTCLQ8',10*0.5,'u');
model15 = changeRxnBounds(model15,'CYOO2pp',0,'b');
model15 = changeRxnBounds(model15,'CYTCHO2',10*0.25,'u');
model15 = changeRxnBounds(model15,'GLYO1',0,'b');
model15 = changeRxnBounds(model15,'3MOBDC',1000,'u');
model15 = changeRxnBounds(model15,'HDAAOR4',0,'u');
model15 = changeRxnBounds(model15,'ODAAOR4',0,'u');
model15 = changeObjective(model15,'EX_2mppal(e)');
fbasol15 = optimizeCbModel(model15);

% 16. max 23BDO from methane, aerobic
model16 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model16 = changeRxnBounds(model16,'RBTDDHy',-1000,'l');
model16 = changeObjective(model16,'EX_23btd-RR(e)');
fbasol16 = optimizeCbModel(model16);

% 17. max 23BDO from methane, microaerobic
model17 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model17 = changeRxnBounds(model17,'EX_meoh(e)',0,'u');
model17 = changeRxnBounds(model17,'CYTCLQ8',0,'l');
model17 = changeRxnBounds(model17,'CYTCLQ8',10*0.5,'u');
model17 = changeRxnBounds(model17,'CYOO2pp',0,'b');
model17 = changeRxnBounds(model17,'CYTCHO2',10*0.25,'u');
model17 = changeRxnBounds(model17,'GLYO1',0,'b');
model17 = changeRxnBounds(model17,'RBTDDHy',-1000,'l');
model17 = changeRxnBounds(model17,'HDAAOR4',0,'u');
model17 = changeRxnBounds(model17,'ODAAOR4',0,'u');
model17 = changeObjective(model17,'EX_23btd-RR(e)');
fbasol17 = optimizeCbModel(model17);

% 18. max 14BDO from methane, aerobic
model18 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model18 = changeRxnBounds(model18,{'OXGDC','GHBDHx','HBUTCT2','HBUTCRy','HBUTDHy'},1000,'u');
model18 = changeObjective(model18,'EX_14bdo(e)');
fbasol18 = optimizeCbModel(model18);

% 19. max 14BDO from methane, microaerobic
model19 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model19 = changeRxnBounds(model19,'EX_meoh(e)',0,'u');
model19 = changeRxnBounds(model19,'CYTCLQ8',0,'l');
model19 = changeRxnBounds(model19,'CYTCLQ8',10*0.5,'u');
model19 = changeRxnBounds(model19,'CYOO2pp',0,'b');
model19 = changeRxnBounds(model19,'CYTCHO2',10*0.25,'u');
model19 = changeRxnBounds(model19,'GLYO1',0,'b');
model19 = changeRxnBounds(model19,'HDAAOR4',0,'u');
model19 = changeRxnBounds(model19,'ODAAOR4',0,'u');
model19 = changeRxnBounds(model19,{'OXGDC','GHBDHx','HBUTCT2','HBUTCRy','HBUTDHy'},1000,'u');
model19 = changeObjective(model19,'EX_14bdo(e)');
fbasol19 = optimizeCbModel(model19);

% 20. max acetate from methane, aerobic
model20 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model20 = changeObjective(model20,'EX_ac(e)');
fbasol20 = optimizeCbModel(model20);

% 21. max acetate from methane, microaerobic
model21 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model21 = changeRxnBounds(model21,'EX_meoh(e)',0,'u');
model21 = changeRxnBounds(model21,'CYTCLQ8',0,'l');
model21 = changeRxnBounds(model21,'CYTCLQ8',10*0.5,'u');
model21 = changeRxnBounds(model21,'CYOO2pp',0,'b');
model21 = changeRxnBounds(model21,'CYTCHO2',10*0.25,'u');
model21 = changeRxnBounds(model21,'GLYO1',0,'b');
model21 = changeRxnBounds(model21,'HDAAOR4',0,'u');
model21 = changeRxnBounds(model21,'ODAAOR4',0,'u');
model21 = changeObjective(model21,'EX_ac(e)');
fbasol21 = optimizeCbModel(model21);

% 22. max lactate from methane, aerobic
model22 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model22 = changeObjective(model22,'EX_lac-D(e)');
fbasol22 = optimizeCbModel(model22);

% 23. max lactate from methane, microaerobic
model23 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model23 = changeRxnBounds(model23,'EX_meoh(e)',0,'u');
model23 = changeRxnBounds(model23,'CYTCLQ8',0,'l');
model23 = changeRxnBounds(model23,'CYTCLQ8',10*0.5,'u');
model23 = changeRxnBounds(model23,'CYOO2pp',0,'b');
model23 = changeRxnBounds(model23,'CYTCHO2',10*0.25,'u');
model23 = changeRxnBounds(model23,'GLYO1',0,'b');
model23 = changeRxnBounds(model23,'HDAAOR4',0,'u');
model23 = changeRxnBounds(model23,'ODAAOR4',0,'u');
model23 = changeObjective(model23,'EX_lac-D(e)');
fbasol23 = optimizeCbModel(model23);

% 24. max pyruvate from methane, aerobic
model24 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model24 = changeObjective(model24,'EX_pyr(e)');
fbasol24 = optimizeCbModel(model24);

% 25. max pyruvate from methane, microaerobic
model25 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model25 = changeRxnBounds(model25,'EX_meoh(e)',0,'u');
model25 = changeRxnBounds(model25,'CYTCLQ8',0,'l');
model25 = changeRxnBounds(model25,'CYTCLQ8',10*0.5,'u');
model25 = changeRxnBounds(model25,'CYOO2pp',0,'b');
model25 = changeRxnBounds(model25,'CYTCHO2',10*0.25,'u');
model25 = changeRxnBounds(model25,'GLYO1',0,'b');
model25 = changeRxnBounds(model25,'HDAAOR4',0,'u');
model25 = changeRxnBounds(model25,'ODAAOR4',0,'u');
model25 = changeObjective(model25,'EX_pyr(e)');
fbasol25 = optimizeCbModel(model25);

% 26. max glycogen from methane, aerobic
model26 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model26 = changeObjective(model26,'DM_glycogen(c)');
fbasol26 = optimizeCbModel(model26);

% 27. max glycogen from methane, microaerobic
model27 = changeRxnBounds(model,'EX_ch4(e)',-10,'l');
model27 = changeRxnBounds(model27,'EX_meoh(e)',0,'u');
model27 = changeRxnBounds(model27,'CYTCLQ8',0,'l');
model27 = changeRxnBounds(model27,'CYTCLQ8',10*0.5,'u');
model27 = changeRxnBounds(model27,'CYOO2pp',0,'b');
model27 = changeRxnBounds(model27,'CYTCHO2',10*0.25,'u');
model27 = changeRxnBounds(model27,'GLYO1',0,'b');
model27 = changeRxnBounds(model27,'HDAAOR4',0,'u');
model27 = changeRxnBounds(model27,'ODAAOR4',0,'u');
model27 = changeObjective(model27,'DM_glycogen(c)');
fbasol27 = optimizeCbModel(model27);

% 28. max ATP from methanol
model28 = changeRxnBounds(model,'EX_meoh(e)',-10,'l');
model28 = changeObjective(model28,'ATPM');
fbasol28 = optimizeCbModel(model28);

% 29. max ATP from ethane
model29 = changeRxnBounds(model,'EX_etn(e)',-10,'l');
model29 = changeRxnBounds(model29,'ALDD2q',1000,'u');
model29 = changeRxnBounds(model29,'ME1',0,'u');
model29 = changeRxnBounds(model29,'OAADCr',0,'u');
model29 = changeObjective(model29,'ATPM');
fbasol29 = optimizeCbModel(model29);

% 30. max biomass from methanol
model30 = changeRxnBounds(model,'EX_meoh(e)',-10,'l');
if findRxnIDs(model30,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model30 = changeObjective(model30,'Biomass_Mcapsulatus_v4_100p0M');
else
    model30 = changeObjective(model30,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol30 = optimizeCbModel(model30);

% 31. max biomass from ethane
model31 = changeRxnBounds(model,'EX_etn(e)',-10,'l');
model31 = changeRxnBounds(model31,'ALDD2q',1000,'u');
model31 = changeRxnBounds(model31,'ME1',0,'u');
model31 = changeRxnBounds(model31,'OAADCr',0,'u');
if findRxnIDs(model31,'Biomass_Mcapsulatus_v4_100p0M') > 0
    model31 = changeObjective(model31,'Biomass_Mcapsulatus_v4_100p0M');
else
    model31 = changeObjective(model31,'Biomass_Mcapsulatus_v3_100p0M');
end
fbasol31 = optimizeCbModel(model31);



% compile results for output
results = [fbasol1.f;
    fbasol2.f;
    fbasol3.f;
    fbasol4.f;
    fbasol5.f;
    fbasol6.f;
    fbasol7.f;
    fbasol8.f;
    fbasol9.f;
    fbasol10.f;
    fbasol11.f;
    fbasol12.f;
    fbasol13.f;
    fbasol14.f;
    fbasol15.f;
    fbasol16.f;
    fbasol17.f;
    fbasol18.f;
    fbasol19.f
    fbasol20.f
    fbasol21.f
    fbasol22.f
    fbasol23.f
    fbasol24.f
    fbasol25.f
    fbasol26.f
    fbasol27.f
    fbasol28.f
    fbasol29.f
    fbasol30.f
    fbasol31.f
    ];


