# McapMetabolicModel

M. capsulatus metabolic model files

### Current Mcap Metabolic Model
* M\_capsulatus\_v1.13\_rBN\_model.mat – The main Matlab metabolic model file with current default constraints.  Load this model into Matlab with COBRA Toolbox or python with cobrapy and use to run FBA and other types of simulations
* M\_capsulatus\_v1.13\_rBN.mat – rBioNet model file.  Load this file into rBioNet (see below) to update and modify.  Save as mat file to produce a cobra model for running simulations.  Contains only UniProt protein IDs, not gene IDs.  Ask Jeff or Cleo why in person.
* M\_capsulatus\_v1.13\_rBN\_RNAseq2\_model.json – a json formatted model that can be loaded by Escher (see below).  This model has been modified from the original model 1.13 by adding dummy heterologous genes (identifiable by “h\_” prefix in gene names) to some reactions.  This is meant to be used for plotting omics data on Escher maps, which often includes heterologous genes.  See omicsmaps description below.  M\_capsulatus\_v1.13\_rBN\_model\_for\_json.mat is a matlab model file with the heterologous gene modifications that has not yet been converted to a .json file.

### Mcap Metabolic Model Supplements
* M\_capsulatus\_v1.13\_rBN.xlsx – spreadsheet of all reactions, metabolites, and genes in the current Mcap metabolic model.  Contains UniProt protein IDs and MCA# gene IDs.
* Mcap Model 1.13 Constraints.xlsx – spreadsheet listing all modified upper and lower bounds set by default in the current model .mat file.  Other tabs in this file contain condition-specific changes to bounds.
* M\_capsulatus\_model\_updates.xlsx – spreadsheet containing detailed notes on manual curation and updates of the current model since early 2013.  At the end is a list of suggested things to curate and analyze for the next model version.
* Mcap Biomass\_v5.xlsx – file used to calculate the metabolic model biomass reaction from multiple experimental datasets.  The latest version (v5) was updated in June 2019 using biomass composition data from PPB219 and PPB220 (metals, S, and P).

### Maps
* Mcap\_Map\_v1.13\_Escher.json – WT Mcap, no heterologous pathways
* Mcap\_Map\_v1.13\_23BDO\_Escher.json – with alsS+budA+butA 23BDO pathway
* Mcap\_Map\_v1.13\_23BDO\_glx\_Escher.json – with 23BDO pathway and glyoxylate cycle
* Mcap\_Map\_v1.13\_23BDO\_actndh\_Escher – with acetoin dehydrogenase pathway to 23BDO
* Mcap\_Map\_v1.13\_23BDO\_serd\_glx2\_Escher.json – with synthetic combined glyoxylate/serine cycle (Hong Yu)
* Mcap\_Map\_v1.13\_isobutanol\_Escher.json – with isobutanol pathway (IEP)
* Mcap\_Map\_v1.13\_14BDO\_Escher.json – with 14BDO pathway (IEP 2)

### Metabolic model update protocol
Use rBioNet to make modifications to the Mcap metabolic model.  More details on rBioNet below.  
https://opencobra.github.io/cobratoolbox/latest/modules/reconstruction/rBioNet/index.html  
See PMID 20057383 for a very detailed description of how to use many different types of experimental data to update a metabolic model and how to test metabolic models

1.  See list of potential model updates or modifications to make in M\_capsulatus\_model\_updates.xlsx.  Keep notes on all proposed model changes and rationale/experimental evidence
2.  Prepare a complete list of all model reactions to add, remove, or modify.  See M\_capsulatus\_model\_updates.xlsx lines 4564-4742 for an example.
3. Check if reactions to add to the model are already in the rBioNet universal reaction database.  If not, add them to a list of reactions to add to the universal database.  Check if all of the metabolites in these reactions are in the rBioNet universal metabolite database.  If not, add them to a list of metabolites to add.  Changes to reaction EC numbers, KEGG rxn IDs, and sometimes notes, references, and confidense scores should be made to the universal database, add these changes to a list.
4. Add all new metabolites to the rBioNet universal database using the rBioNet Reaction and Metabolite Editor GUI or text file import.  Then add all new reactions using the GUI or text file import.  rBioNet will check that reactions are mass and charge balanced and that they are unique.  rBioNet does not let you change most rxn properties, so to make these kinds of modifications, close rBioNet, load the universal database .mat file into matlab and edit manually, then save the file.
5. After making all updates to the rBioNet universal metabolite and reaction databases, next edit the Mcap model file.  Open the rBioNet Reconstruction Creator GUI, load the Mcap model rBioNet file (M\_capsulatus\_v1.13\_rBN.mat is the latest version), and edit.  First remove reactions, then add new reactions from the universal database, and make reaction modifications last.  For changes to reactions in the universal database that were made in step 4, remove the reaction from the Mcap model and then add the updated reaction from the universal database back in.  Model-specific reaction changes such as GPRs and notes can be made without removing and reading reactions.
6. Save the updated rBioNet model file with a new filename, then export the model as a COBRA model (I usually append \_model to the filename) that can be loaded into matlab or python to run simulations.
7. Test the updated model and compare results to the previous model version.  See standard\_tests\_Mcap\_model.m for the tests I ran when updating the model to version 1.13.  See M\_capsulatus\_model\_updates.xlsx lines 4751-4783 for the model test results last time.
8. Update the Escher metabolic network map if any of the reactions on it were changed.
9. Update the list of default and condition specific constraints in Mcap Model 1.13 Constraints.xlsx if any of these were changed.
10. [optional]  Run the general\_characterization…m scripts and record the results in Mcap model 1.13 general characterization.xlsx to produce a handy reference file of model fluxes, yields, essential genes, etc.

